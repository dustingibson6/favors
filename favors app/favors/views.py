from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.sessions.models import Session
from django.db.models import Max
from django.db.models import Q
import forms, email_client, models, os, uuid, globals, traceback, json, time, re
from cStringIO import StringIO
from PIL import Image
from datetime import time
from datetime import datetime

def index(request):
	return HttpResponse("Hey hey hey!")

#TODO: Require csrf token
@csrf_exempt
def registration(request):
	if request.method == 'POST':
		try:
			user_form = forms.RegistrationForm(data=request.POST)
			#print user_form.cleaned_data['username']
			personal_info_form = forms.PersonalInfoForm(data=request.POST)
			username_data = request.POST.get('username')
			password_data = request.POST.get('password')
			email_data = request.POST.get('email')
			zip_data = request.POST.get('zipcode')
			#TODO: Seperate function for android client and seperate function for web client
			try:
				existing_user = User.objects.filter(username=username_data)
				existing_email = User.objects.filter(email=email_data)
				if len(existing_user ) == 0 and len(existing_email) == 0:
					email_client_response = email_client.email_client(email_data).send_reg_mail()
					#User -> UserProfile
					#UserProfile -> PersonalInfo and UserGroup (Registered)
					registered_user_group = models.UserGroup.objects.get(name='Registered')
					new_user = User.objects.create_user(username_data, email_data)
					new_user.set_password(password_data)
					new_personal_info = models.PersonalInfo(zip=zip_data)
					new_personal_info.save()
					new_user_profile = models.UserProfile(ldap_user=new_user,user_group_fk=registered_user_group,username=username_data,personal_info_fk=new_personal_info)
					new_user_profile.save()
					new_profile_photo = models.ProfileImage(personal_info_fk=new_personal_info,image_path='default')
					print new_profile_photo
					new_profile_photo.save()
					new_user.save()
					return HttpResponse("Success")
				else:
					print "Bad email or username"
					return HttpResponse("Email and/or Username Exists")
			except:
				traceback.print_exc()
				return HttpResponse("Something is wrong")
		except:
			traceback.print_exc()
			return HttpResponse("Something is wrong")
	else:
		user_form = forms.RegistrationForm()
		personal_info_form = forms.PersonalInfoForm()
	if request.user.is_authenticated():
		return HttpResponse("Already logged in")
	else:
		return render(request,'favors/registration.html',
		{'user_form': user_form, 'personal_info_form' : personal_info_form})
		
@csrf_exempt
def user_info(request):
	if request.method == 'GET':
		if request.user.is_authenticated():
			return HttpResponse("Logged in")
		else:
			return HttpResponse("Not Logged in")
	
@csrf_exempt
def login_user(request):
	json_message = []
	if request.method == 'POST':
		try:
			login_form = forms.LoginForm(data=request.POST)
			username = request.POST.get('username')
			password = request.POST.get('password')
			user = authenticate(username=username,password=password)
			existing_user = User.objects.filter(username=username)
			zip = models.UserProfile.objects.filter(ldap_user_id=existing_user)[0].personal_info_fk.zip
			if user:
				login_info = {}
				login(request,user)
				login_info['result']='Success'
				login_info['username']=username
				login_info['userid'] = str(user.id)
				login_info['zip'] = zip
				login_info['session'] = str(request.session.session_key)
				json_message.append(login_info)
				return HttpResponse(json.dumps(json_message))
			else:
				return HttpResponse("Username and Password Does not Match")
		except:
			traceback.print_exc()
			return HttpResponse("can't get personal info")
	else:
		login_form = forms.LoginForm()
		return render(request,'favors/login.html',{'login_form':login_form})
		
@csrf_exempt
def user_profile(request):
	if request.method == 'POST':
		if request.user.is_authenticated():
			profile_form = forms.ProfileForm(request.POST,request.FILES)
			logout_value = request.POST.get("logout")
			if logout_value == "logout":
				logout(request)
				return HttpResponse("Logout")
			try:
				existing_user = User.objects.filter(username=request.user.username)
				first_name = request.POST.get('firstName')
				last_name = request.POST.get('lastName')
				qualifications = request.POST.get('qualifications')
				zip = request.POST.get('zip')
				#TODO: First Name and Last Name Verification
				try: 
					existing_user_profile = models.UserProfile.objects.filter(ldap_user_id=request.user.id)
					#TODO: Check for user groups if needed
					try:
						existing_personal_info = existing_user_profile[0].personal_info_fk
						existing_personal_info.last_name = last_name
						existing_personal_info.first_name = first_name
						existing_personal_info.zip = zip
						existing_personal_info.qualifications = qualifications
						existing_personal_info.save()
						return HttpResponse('Sent')
					except:
						return HttpResponse("Personal Info does not exist")
				except:
					return HttpResponse("User Profile does not exists")
			except:
				return HttpResponse("User does not exists")
		else:
			return HttpResponse('Not Logged In')
	else:
		profile_form = forms.ProfileForm()
		if request.user.is_authenticated():
			return render(request,'favors/profile.html',
			{'profile_form': profile_form})
		else:
			return HttpResponse("Not Logged In")
			
@csrf_exempt			
def upload_profile_photo(request):
	if request.method == 'POST':
		if request.user.is_authenticated():
			photo_form = forms.UploadPhotoForm(request.POST,request.FILES)
			existing_user = User.objects.filter(username=request.user.username)
			try:
				existing_user_profile = models.UserProfile.objects.filter(ldap_user_id=request.user.id)
				try:
					#TODO: Check for user groups if needed
					#existing_personal_info = models.PersonalInfo.objects.filter(personal_info_id=existing_user_profile[0].personal_info_fk)
					existing_photo = models.ProfileImage.objects.filter(personal_info_fk=existing_user_profile[0].personal_info_fk)
					image_id = None
					if 'photoFile' in request.FILES:
						print "digging into request"
						image_id = upload_photo(request.FILES['photoFile'])
					if image_id != None:
						new_image = models.ProfileImage(personal_info_fk=existing_user_profile[0].personal_info_fk,image_path=image_id)
						if len(existing_photo) > 0:
							delete_photo(existing_photo[0].image_path)
							existing_photo[0].delete()
						new_image.save()
					#existing_personal_info.save()
					return HttpResponse('Sent')
				except:
					traceback.print_exc()
					return HttpResponse("User Profile does not exists")
			except:
				traceback.print_exc()
				return HttpResponse("User does not exists")
		else:
			return HttpResponse('Not Logged In')
	else:
		photo_form = forms.UploadPhotoForm()
		if request.user.is_authenticated():
			return render(request,'favors/upload_profile_photo.html',
			{'upload_profile_photo': photo_form})
		else:
			return HttpResponse("Not Logged In")
	
@csrf_exempt	
def get_personal_info(request):
	json_message = []
	if request.method == 'GET':
		if request.user.is_authenticated():
			try:
				session = Session.objects.get(session_key=request.session.session_key)
				user_id = session.get_decoded().get('_auth_user_id')
				existing_user_profile = models.UserProfile.objects.filter(ldap_user_id=user_id)
				existing_personal_info = existing_user_profile[0].personal_info_fk
				existing_photo = models.ProfileImage.objects.get(personal_info_fk=existing_personal_info)
				profile_location = models.PresetLocation.objects.filter(presetlocation_id=existing_personal_info.zip)[0]
				personal_info = {}
				personal_info['location'] = profile_location.city + ", " + profile_location.state + " (" + existing_personal_info.zip + ")"
				personal_info['lastname'] = existing_personal_info.last_name
				personal_info['firstname'] = existing_personal_info.first_name
				personal_info['qualifications'] = existing_personal_info.qualifications
				personal_info['imagepath'] = existing_photo.image_path
				json_message.append(personal_info)
				return HttpResponse(json.dumps(json_message))
			except:
				traceback.print_exc()
				return HttpResponse("Cannot retrieve profile info")
		else:
			return HttpResponse("Not Logged in")
	else:	
		return HttpResponse("Wrong method")

@csrf_exempt
def delete_session(request):
	if request.method == 'GET':
		try:
			logout(request)
			return HttpResponse("Logged Out")
		except:
			return HttpResponse("Already Logged Out")
	return HttpResponse("Wrong Method")

@csrf_exempt			
def get_chat_list(request):
	json_message = []
	if request.method == 'GET':
		print "GETTING CHAT LIST"
		if request.user.is_authenticated():
			session = Session.objects.get(session_key=request.session.session_key)
			user_id = session.get_decoded().get('_auth_user_id')
			existing_user = User.objects.get(pk=user_id)
			existing_profile = models.UserProfile.objects.filter(ldap_user=existing_user)
			user_dict = {}
			try:
				existing_contacts = models.Contact.objects.filter(user_a_fk=existing_profile)
				if existing_contacts.exists():
					for existing_contact in existing_contacts:
						recieved = {}
						latest_message = existing_contact.message_fk
						sender_user = existing_contact.user_b_fk
						existing_photo = models.ProfileImage.objects.filter(personal_info_fk=sender_user.personal_info_fk)[0]
						if latest_message == None:
							recieved['message'] = ''
							recieved['date'] = None
						else:
							recieved['message'] = latest_message.message_content
							recieved['date'] = latest_message.date_sent.strftime('%B %d, %Y %I:%M %p')
						recieved['sender'] = str(sender_user.ldap_user)
						recieved['image'] = existing_photo.image_path
						recieved['firstname'] = sender_user.personal_info_fk.first_name
						recieved['lastname'] = sender_user.personal_info_fk.last_name
						user_dict[str(sender_user)] = recieved
						json_message.append(recieved)
				else:
					return HttpResponse('no contacts')
				# for key in user_dict:
					# json_message.append(user_dict[key])
				return HttpResponse(json.dumps(json_message))
			except:
				traceback.print_exc()
				return HttpResponse("No chats")
			return HttpResponse("User does not exists")
		else:
			return HttpResponse("Not logged in")
	else:
		return HttpResponse("Wrong method")
			
@csrf_exempt
def get_chat_count(request):
	if request.method == 'GET':
		if request.user.is_authenticated():
			session = Session.objects.get(session_key=request.session.session_key)
			user_id = session.get_decoded().get('_auth_user_id')
			existing_user = User.objects.get(pk=user_id)
			try:
				try:
					chat_count = str(len(models.Message.objects.filter(recipient_fk_id=existing_user,recieved='F')))
					return HttpResponse(chat_count)
				except:
					return HttpResponse("0")
			except:
				return HttpResponse("User does not exists")
				
@csrf_exempt
def get_chat_users(request):
	json_message = []
	if request.method == 'GET':
		if request.user.is_authenticated():
			session = Session.objects.get(session_key=request.session.session_key)
			user_id = session.get_decoded().get('_auth_user_id')
			existing_user = User.objects.get(pk=user_id)
			try:
				try:
					message_users = models.Message.objects.filter(recipient_fk_id=existing_user,recieved='F').values_list('sender_fk').distinct()				
					#message_user = models.Message.objects.all().distinct()
					for message_user in message_users:
						try:
							user_info = {}
							existing_user = User.objects.get(pk=int(message_user[0]))
							existing_user_profile = models.UserProfile.objects.filter(ldap_user=existing_user.id)
							user_info['username'] = existing_user.username
							user_info['firstname'] = existing_user_profile[0].personal_info_fk.first_name
							user_info['lastname'] = existing_user_profile[0].personal_info_fk.last_name
							json_message.append(user_info)
						except:
							pass
					return HttpResponse(json.dumps(json_message))
				except:
					traceback.print_exc()
					return HttpResponse("User does not exist")
			except:
				return HttpResponse("User does not exists")
					

@csrf_exempt				
def chat(request):
	recipient = request.POST.get('recipient')
	message = request.POST.get('message')
	if request.method == 'POST':
		if request.user.is_authenticated():
			try:
				session = Session.objects.get(session_key=request.session.session_key)
				user_id = session.get_decoded().get('_auth_user_id')
				existing_user = User.objects.get(pk=user_id)
				try:
					print recipient
					existing_recipient = models.User.objects.get(username=recipient)
					try:
						new_message = models.Message(recipient_fk=existing_recipient,sender_fk=request.user,message_content=message,recieved='F')
						new_message.save()
						updated_contact_a = models.Contact.objects.filter(user_a_fk=existing_recipient,user_b_fk=request.user)
						return HttpResponse("Sent")
					except:
						traceback.print_exc()
						return  HttpResponse("cannot send message")
				except:
					traceback.print_exc()
					return HttpResponse("Cannot find recipient")
			except:
				return HttpResponse("Cannot find sender user")
		else:
			return HttpResponse("Not logged in")
	else:
		chat_form = forms.ChatForm()
		return render(request,'favors/chat.html',{'chat_form':chat_form})
		
@csrf_exempt
def create_favor(request):
	title = request.POST.get('title')
	description = request.POST.get('description')
	reward = request.POST.get('reward')
	zip = request.POST.get('zip')
	number_of_participants = request.POST.get('number_of_participants')
	creater = request.POST.get('creater')
	duration = request.POST.get('duration')
	deadline = request.POST.get('deadline')
	deadline_date = datetime.strptime(deadline, '%m/%d/%Y %I:%M %p')
	if request.method == 'POST':
		if request.user.is_authenticated():
			try:
				session = Session.objects.get(session_key=request.session.session_key)
				user_id = session.get_decoded().get('_auth_user_id')
				existing_user = User.objects.get(pk=user_id)
				try:
					existing_user_profile = models.UserProfile.objects.filter(ldap_user=existing_user.id)
					new_favor = models.Favor(description=description,title=title,reward=reward,location_fk_id=zip,number_of_participants=number_of_participants,creator_fk=existing_user_profile[0],duration=duration,deadline=deadline_date)
					new_favor.save()
					return HttpResponse(new_favor.favor_id)
				except:
					traceback.print_exc()
					return HttpResponse("Cannot create favors")
			except:
				traceback.print_exc()
				return HttpResponse("Cannot find user")
		else:
			return HttpResponse("Not logged in")
	else:
		new_favor_form = forms.NewFavorForm()
		return render(request,'favors/new_favor.html',{'new_favor_form':new_favor_form})
		
@csrf_exempt
def update_or_create_candidate(request):
	if request.method == 'POST':
		if request.user.is_authenticated():
			try:
				existing_user = User.objects.filter(username=request.user.username)
				existing_profile = models.UserProfile.objects.filter(ldap_user=existing_user)[0]
				favor_id = request.POST.get('favorid')
				sent_status = request.POST.get('status')
				print sent_status
				existing_favor = models.Favor.objects.filter(favor_id=int(favor_id))
				if existing_favor.exists():
					existing_candidate = models.Candidate.objects.filter(candidate_fk=existing_profile,favor_fk=existing_favor[0])
					if existing_candidate.exists():
						current_candidate = existing_candidate[0]
						print "candidate exist, " + sent_status
						current_candidate.status = sent_status
						current_candidate.save()
						return HttpResponse('Success')
					else:
						new_candidate = models.Candidate(favor_fk=existing_favor[0],status=sent_status,candidate_fk=existing_profile)
						new_candidate.save()
						return HttpResponse('Success')
				else:
					return HttpResponse('Favor does not exist')
			except:
				traceback.print_exc()
				return HttpResponse('Cannot get data')
		else:
			return HttpResponse('Not logged in')
	else:
		return HttpResponse('Wrong method')
		
@csrf_exempt
def add_contact(request):
	user_b_username = request.POST.get('username')
	if request.method == 'POST':
		if request.user.is_authenticated():
			try:
				existing_user = User.objects.filter(username=request.user.username)
				existing_user_a = User.objects.filter(username=request.user.username)
				existing_profile_a = models.UserProfile.objects.filter(ldap_user=existing_user)[0]
				existing_user_b = User.objects.filter(username=user_b_username)
				existing_profile_b = models.UserProfile.objects.filter(ldap_user=existing_user_b)[0]
				existing_contact = models.Contact.objects.filter(user_a_fk=existing_profile_a,user_b_fk=existing_profile_b)
				if existing_contact.exists():
					return HttpResponse("Contact already exists")
				else:
					new_contact_a = models.Contact(user_a_fk=existing_profile_a,user_b_fk=existing_profile_b)
					new_contact_b = models.Contact(user_a_fk=existing_profile_b,user_b_fk=existing_profile_a)
					new_contact_a.save()
					new_contact_b.save()
			except:
				traceback.print_exc()
				return HttpResponse("Error getting users")
		else:
			return HttpResponse("Not logged in")
	else:
		return HttpResponse("Wrong method")
		
@csrf_exempt			
def upload_favor_photo(request):
	if request.method == 'POST':
		if request.user.is_authenticated():
			try:
				favor_id = request.COOKIES.get('favor_id')
				existing_user = User.objects.filter(username=request.user.username)
				try:
					#TODO: Check for user groups if needed
					#existing_personal_info = models.PersonalInfo.objects.get(personal_info_id=existing_user_profile)
					existing_favor = models.Favor.objects.filter(favor_id=int(favor_id))
					try:
						image_id = None
						if 'photoFile' in request.FILES:
							image_id = upload_photo(request.FILES['photoFile'])
						if image_id != None:
							new_image = models.FavorImage(favor_fk=existing_favor[0],image_path=image_id)
							#print existing_photo
							# if len(existing_photo) > 0:
								# delete_photo(existing_photo[0].image_path)
								# existing_photo[0].delete()
							new_image.save()
						return HttpResponse('Sent')
					except:
						traceback.print_exc()
						return HttpResponse("Cannot upload photo")
				except:
					traceback.print_exc()
					return HttpResponse("Favor does not exist")
			except:
				traceback.print_exc()
				return HttpResponse("User does not exists")
		else:
			return HttpResponse('Not Logged In')
	else:
		return HttpResponse('Wrong Method')

@csrf_exempt
def get_profile(request):
	json_message = []
	if request.method == 'GET':
		if request.user.is_authenticated():
			try:
				view_user_id = request.COOKIES.get('profile')
				existing_user = User.objects.filter(username=view_user_id)
				print existing_user
				try:
					existing_profile = models.UserProfile.objects.filter(ldap_user=existing_user)
					#print existing_profile[0].personal_info_fk.first_name
					existing_personal_info = existing_profile[0].personal_info_fk
					try:
						user_info = {}
						user_info['firstname'] = existing_personal_info.first_name
						user_info['lastname'] = existing_personal_info.last_name
						try:
							preset_location = models.PresetLocation.objects.filter(zip=existing_personal_info.zip)
							user_info['zip'] = preset_location[0].city + ", " + preset_location[0].state
						except:
							traceback.print_exc()
							user_info['zip'] = existing_personal_info.zip
						user_info['qualifications'] = existing_personal_info.qualifications
						try:
							existing_photo = models.ProfileImage.objects.get(personal_info_fk=existing_personal_info)
							user_info['photo_id'] = existing_photo.image_path
						except:
							traceback.print_exc()
							user_info['photo_id'] = 'default'
						json_message.append(user_info)
					except:
						traceback.print_exc()
						return HttpResponse("Unable to get info")
					return HttpResponse(json.dumps(json_message))
				except:
					traceback.print_exc()
					return HttpResponse("Profile does not exists")
			except:
				print "User does not exist"
				return HttpResponse("User does not exists")
		else:
			print "not logged in"
			return HttpResponse("Not logged in")
			
@csrf_exempt
def auto_location(request):
	json_message = []
	if request.method == 'GET':
		try:
			type_city = request.COOKIES.get('location')
			mod_type_city = type_city.strip().lower()
			if mod_type_city[0].isdigit() == True:
				mod_type_city = re.sub(r'[a-zA-Z]','', mod_type_city)
				location_candidates = models.PresetLocation.objects.filter(presetlocation_id__contains=mod_type_city)[:100]
			else:
				if mod_type_city.find(',') >= 0:
					sel_city = mod_type_city.split(',')[0]
					sel_alt = mod_type_city.split(',')[1]
					location_candidates = models.PresetLocation.objects.filter(city__icontains=sel_city,state__icontains=sel_alt)[:100]
				else:
					location_candidates = models.PresetLocation.objects.filter(city__icontains=mod_type_city)[:100]
			location_list = []
			if location_candidates.exists():
				print "Exists!"
				for location_candidate in location_candidates:
					loc_string = {}
					loc_zip = location_candidate.presetlocation_id
					loc_state = location_candidate.state
					loc_city = location_candidate.city
					loc_string['location'] = loc_city + ", " + loc_state + " (" + loc_zip + ")"
					json_message.append(loc_string)
			return HttpResponse(json.dumps(json_message))
		except:
			traceback.print_exc()
			return HttpResponse("Can't get location")
	else:
		return HttpResponse("Wrong method")
			
@csrf_exempt
def get_favor(request):
	json_message = []
	if request.method == 'GET':
		if request.user.is_authenticated():
			try:
				distance = '0'
				location_zip = request.COOKIES.get('zip')
				current_favor_id = request.COOKIES.get('favorid')
				existing_user = User.objects.filter(username=request.user.username)
				existing_profile = models.UserProfile.objects.filter(ldap_user=existing_user)[0]
				session = Session.objects.get(session_key=request.session.session_key)
				current_favors = models.Favor.objects.filter(pk=int(current_favor_id))
				if current_favors.exists():
					current_favor = current_favors[0]
					if location_zip != current_favor.location_fk_id:
						current_location_distance = models.LocationDistance.objects.filter(from_location_fk_id=location_zip,to_location_fk=current_favor.location_fk)[0]
						distance = str(current_location_distance.distance)
					current_location = models.PresetLocation.objects.filter(pk=current_favor.location_fk_id)[0]
					existing_candidate = models.Candidate.objects.filter(favor_fk = current_favor, candidate_fk = existing_profile)
					author_profile = models.UserProfile.objects.filter(user_profile_id=current_favor.creator_fk_id)[0]
					all_images = models.FavorImage.objects.filter(favor_fk=current_favor)
					favors_dump = {}
					favors_dump['title'] = current_favor.title
					favors_dump['author_username'] = author_profile.ldap_user.username
					favors_dump['author_firstname'] = author_profile.personal_info_fk.first_name
					favors_dump['author_lastname'] = author_profile.personal_info_fk.last_name
					favors_dump['description'] = current_favor.description
					favors_dump['reward'] = current_favor.reward
					favors_dump['zip'] = str(current_favor.location_fk_id)
					favors_dump['city']  = current_location.city
					favors_dump['state'] = current_location.state
					favors_dump['distance'] = distance
					favors_dump['deadline'] = current_favor.deadline.strftime('%m/%d/%Y %I:%M %p')
					if existing_candidate.exists():
						favors_dump['status'] = existing_candidate[0].status
					else:
						favors_dump['status'] = 'None'
					image_list = []
					for current_image in all_images:
						image_dict = {}
						image_dict['image'] = current_image.image_path
						print current_image.image_path
						image_list.append(image_dict)
					favors_dump['images'] = image_list
					json_message.append(favors_dump)
					return HttpResponse(json.dumps(json_message))
				else:
					HttpResponse("Favor do not exist")
			except:
				traceback.print_exc()
				HttpResponse("Cannot find info")
		else:
			HttpResponse("Not logged in")
	else:
		HttpResponse("wrong method")

@csrf_exempt
def get_favors_list(request):
	json_message = []
	if request.method == 'GET':
		if request.user.is_authenticated():
			try:
				location_zip = request.COOKIES.get('zip')
				session = Session.objects.get(session_key=request.session.session_key)
				user_id = session.get_decoded().get('_auth_user_id')
				existing_user = User.objects.get(pk=user_id)
				existing_profile = models.UserProfile.objects.filter(ldap_user=existing_user)[0]
				nearby_locations = models.LocationDistance.objects.filter(from_location_fk_id=location_zip).order_by('distance')
				onlocation_favors = models.Favor.objects.filter(location_fk_id=location_zip).exclude(creator_fk=existing_profile)
				for nearby_location in nearby_locations:
					try:
						current_city = models.PresetLocation.objects.filter(presetlocation_id=nearby_location.to_location_fk_id)[0]
						current_favors = models.Favor.objects.filter(location_fk_id=nearby_location.to_location_fk).exclude(creator_fk=existing_profile)
						for current_favor in current_favors:
							thumb_images = models.FavorImage.objects.filter(favor_fk=current_favor)
							current_candidate = models.Candidate.objects.filter(favor_fk=current_favor,candidate_fk=existing_profile)
							if thumb_images.exists():
								thumb_image = thumb_images[0]
							else:
								thumb_image = None
							favors_dump = {}
							favors_dump['favorid'] = str(current_favor.favor_id)
							favors_dump['title'] = current_favor.title
							favors_dump['zip'] = str(current_favor.location_fk_id)
							favors_dump['city'] = current_city.city
							favors_dump['state'] = current_city.state
							favors_dump['distance'] = str(nearby_location.distance)
							if current_candidate.exists():
								favors_dump['status'] = current_candidate[0].status
							else:
								favors_dump['status'] = 'None'
							if thumb_image == None:
								favors_dump['thumb'] = 'None'
							else:
								favors_dump['thumb'] = thumb_image.image_path
							favors_dump['date'] = current_favor.date_created.strftime('%B %d, %Y %I:%M %p')
							favors_dump['deadline'] = current_favor.deadline.strftime('%m/%d/%Y %I:%M %p')
							json_message.append(favors_dump)
					except:
						print "ERROR WITH FAVOR LIST"
						traceback.print_exc()
						return HttpResponse("Issue retrieving data")
				current_city = models.PresetLocation.objects.filter(presetlocation_id=location_zip)[0]
				for onlocation_favor in onlocation_favors:
					thumb_images = models.FavorImage.objects.filter(favor_fk=onlocation_favor)
					current_candidate = models.Candidate.objects.filter(favor_fk=onlocation_favor,candidate_fk=existing_profile)
					if thumb_images.exists():
						thumb_image = thumb_images[0]
					else:
						thumb_image = None
					favors_dump = {}
					favors_dump['favorid'] = str(onlocation_favor.favor_id)
					favors_dump['title'] = onlocation_favor.title
					favors_dump['zip'] = str(onlocation_favor.location_fk_id)
					favors_dump['city'] = current_city.city
					favors_dump['state'] = current_city.state
					favors_dump['distance'] = '0'
					if thumb_image == None:
						favors_dump['thumb'] = 'None'
					else:
						favors_dump['thumb'] = thumb_image.image_path
					if current_candidate.exists():
						favors_dump['status'] = current_candidate[0].status
					else:
						favors_dump['status'] = 'None'
					favors_dump['date'] = onlocation_favor.date_created.strftime('%B %d, %Y %I:%M %p')
					favors_dump['deadline'] = onlocation_favor.deadline.strftime('%m/%d/%Y %I:%M %p')
					json_message.append(favors_dump)
				return HttpResponse(json.dumps(json_message))
			except:
				traceback.print_exc()
				return HttpResponse("Issue getting location")
		else:
			return HttpResponse("User not logged in")
	else:
		return HttpResponse("Wrong method")
		
@csrf_exempt
def get_favors_and_candidates(request):
	json_message = []
	if request.method == 'GET':
		if request.user.is_authenticated():
			try:
				session = Session.objects.get(session_key=request.session.session_key)
				user_id = session.get_decoded().get('_auth_user_id')
				existing_user = User.objects.get(pk=user_id)
				existing_profile = models.UserProfile.objects.filter(ldap_user=existing_user)[0]
				my_favors = models.Favor.objects.filter(creator_fk=existing_profile)
				for my_favor in my_favors:
					favor_image = models.FavorImage.objects.filter(favor_fk=my_favor)[0]
					current_favor = {}
					current_favor['favorid'] = str(my_favor.pk)
					current_favor['title'] = my_favor.title
					current_favor['deadline'] = my_favor.deadline.strftime('%m/%d/%Y %I:%M %p')
					current_favor['favorimage'] = favor_image.image_path
					my_candidates = models.Candidate.objects.filter(favor_fk=my_favor,status='Interested')
					all_candidates = []
					for my_candidate in my_candidates:
						current_candidate = {}
						candidate_user = my_candidate.candidate_fk
						candidate_info = my_candidate.candidate_fk.personal_info_fk
						candidate_image = models.ProfileImage.objects.filter(personal_info_fk=candidate_info)[0]
						current_candidate['userid'] = str(candidate_user.pk)
						current_candidate['firstname'] = candidate_info.first_name
						current_candidate['lastname'] = candidate_info.last_name
						current_candidate['username'] = candidate_user.username
						current_candidate['candidateimage'] = candidate_image.image_path
						all_candidates.append(current_candidate)
					current_favor['candidates'] = all_candidates
					json_message.append(current_favor)
				return HttpResponse(json.dumps(json_message))
			except:
				traceback.print_exc()
				return HttpResponse("Issue getting data")
		else:
			return HttpResponse("Not Logged in")
	else:
		return HttpResponse("Wrong method")
			
def calculate_distance(position_a, position_b):
	lat1 = math.radians(position_a[0])
	lat2 = math.radians(position_b[0])
	lon1 = math.radians(position_a[1])
	lon2 = math.radians(position_b[1])
	delta_lat = lat2-lat1
	delta_lon = lon2-lon1
	angle = (math.sin(delta_lat/2))**2 + math.cos(lat1) * math.cos(lat2) * (math.sin(delta_lon/2))**2 
	c = 2 * math.atan2(math.sqrt(angle) , math.sqrt(1-angle))
	return 3959* c

def upload_photo(file_to_upload):
	print "uploading photo..."
	photo_id = str(uuid.uuid4().get_hex().upper()[0:20])
	chunk = file_to_upload.read()
	print len(chunk)
	with open(  globals.content_path + photo_id + ".png", 'wb+') as dest:
		dest.write(chunk)
	with open(  globals.thumb_path + photo_id + ".png", 'wb+') as dest:
		try:
			thumb_image = Image.open(StringIO(chunk))
			thumb_image = thumb_image.resize((100,100),Image.BILINEAR)
			thumb_image.save(globals.thumb_path + photo_id + ".png")
		except:
			traceback.print_exc()
	return photo_id
	
def delete_photo(photo_id):
	if photo_id == "default":
		return
	try:
		os.remove(globals.content_path + photo_id + ".png")
	except:
		traceback.print_exc()