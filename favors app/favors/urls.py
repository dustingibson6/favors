from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'registration.html', views.registration, name='registration'),
	url(r'login.html', views.login_user, name='login'),	
	url(r'profile.html', views.user_profile, name='profile'),
	url(r'userinfo.html', views.user_info, name='user_info'),
	url(r'get_personal_info.html', views.get_personal_info, name='get_personal_info'),
	url(r'upload_profile_photo.html', views.upload_profile_photo, name='upload_profile_photo'),
	url(r'delete_session.html', views.delete_session, name='delete_session'),
	url(r'chat.html',views.chat,name='chat'),
	url(r'get_chat_list.html',views.get_chat_list,name='get_chat_list'),
	url(r'get_chat_count.html',views.get_chat_count,name='get_chat_count'),
	url(r'get_chat_users.html',views.get_chat_users,name='get_chat_users'),
	url(r'new_favor.html',views.create_favor,name='create_favor'),
	url(r'get_pinfo.html',views.get_profile,name='get_pinfo'),
	url(r'upload_favor_photo.html',views.upload_favor_photo,name='upload_favor_photo'),
	url(r'get_favors_list.html',views.get_favors_list,name='get_favors_list'),
	url(r'get_favor.html',views.get_favor,name='get_favor'),
	url(r'auto_location.html',views.auto_location,name='auto_location'),
	url(r'update_or_create_candidate.html',views.update_or_create_candidate,name='update_or_create_candidate'),
	url(r'add_contact.html',views.add_contact,name='add_contact'),
	url(r'get_favors_and_candidates.html',views.get_favors_and_candidates,name='get_favors_and_candidates'),
]