from django.contrib.auth.models import User
from django import forms
import models

class RegistrationForm(forms.ModelForm):
	password = forms.CharField(label='Password:', widget=forms.PasswordInput())
	username = forms.CharField(label='Username:', max_length=30)
	email = forms.CharField(label='Email:', max_length=30)
	
	class Meta:
		model = User
		fields = ('username','password','email')
		
class PersonalInfoForm(forms.ModelForm):
	firstName = forms.CharField(label='First Name:', max_length=30)
	lastName = forms.CharField(label='Last Name:', max_length=30)
	zipcode = forms.CharField(label='Zipcode:',max_length=30)
	qualifications = forms.CharField(label='Qualifications:',max_length=1000)
	#dateOfBirth = forms.CharField(label='Date of Birth:', max_length=30)
	
	class Meta:
		model = models.PersonalInfo
		fields = ('firstName','lastName','qualifications')
		
class LoginForm(forms.ModelForm):
	username = forms.CharField(label='Username:', max_length=30)
	password = forms.CharField(label='Password:', widget=forms.PasswordInput())
	
	class Meta:
		model = User
		fields = ('username','password')
		
class ProfileForm(forms.ModelForm):
	firstName = forms.CharField(label='First Name:', max_length=30)
	lastName = forms.CharField(label='Last Name:', max_length=30)
	photoFile = forms.FileField()
	title = forms.CharField(max_length=50)
	logout = forms.CharField(widget=forms.HiddenInput())
	qualifications = forms.CharField(label='Qualifications:',max_length=1000)
	
	class Meta:
		model = models.PersonalInfo
		fields = ('firstName','lastName')
		
class UploadPhotoForm(forms.ModelForm):
	photoFile = forms.FileField()

	class Meta:
		model = models.PersonalInfo
		fields = '__all__'
		
class ChatForm(forms.ModelForm):
	recipient = forms.CharField(label='recipient:', max_length=30)
	message = forms.CharField(label='Message:',max_length=1000)
	
	class Meta:
		model = models.Message
		fields = '__all__'
		
class NewFavorForm(forms.ModelForm):
	title = forms.CharField(label='Title:',max_length=30)
	duration = forms.CharField(label='Duration:',max_length=30)
	description = forms.CharField(label='Description:',max_length=1000)
	rewards = forms.CharField(label='Rewards:',max_length=1000)
	zip = forms.CharField(label='Zip:',max_length=20)
	number_of_participants = forms.CharField(label='Number of Participants:',max_length=20)
	
	class Meta:
		model = models.Favor
		fields = '__all__'
		
	