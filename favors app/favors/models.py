from django.db import models
from django.contrib.auth.models import User

class PresetLocation(models.Model):
    presetlocation_id = models.CharField(max_length=6,primary_key=True)
    state = models.CharField(max_length=3,null=True)
	address = models.CharField(max_length=50,null=True)
	apartment_number = models.CharField(max_length=20,null=True)
	city = models.CharField(max_length=30,null=True)
	zip = models.CharField(max_length=20,null=True)
	country = models.CharField(max_length=30,null=True)
	latitude = models.DecimalField(max_digits=5,decimal_places=2,null=True)
	longitude = models.DecimalField(max_digits=5,decimal_places=2,null=True)

class PersonalInfo(models.Model):
	personal_info_id = models.AutoField(primary_key=True)
	date_of_birth = models.CharField(max_length=30,null=True)
	first_name = models.CharField(max_length=30,null=True)
	last_name = models.CharField(max_length=30,null=True)
	zip = models.CharField(max_length=20,null=True)
	qualifications = models.CharField(max_length=1000,null=True)
	
class UserGroup(models.Model):
	user_group_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=15,null=True)
	
class UserProfile(models.Model):
	ldap_user = models.OneToOneField(User)
	user_profile_id = models.AutoField(primary_key=True)
	username = models.CharField(max_length=30,null=True)
	user_profile_password = models.CharField(max_length=30,null=True)
	user_group_fk = models.ForeignKey(UserGroup)
	personal_info_fk = models.ForeignKey(PersonalInfo)
	
class FailedEmail(models.Model):
	failed_email_id = models.AutoField(primary_key=True)
	user_profile_fk = models.ForeignKey(UserProfile)
	
class ProfileImage(models.Model):
	profile_image_id = models.AutoField(primary_key=True)
	personal_info_fk = models.ForeignKey(PersonalInfo)
	image_path = models.CharField(max_length=30,null=True)
	
class Message(models.Model):
	message_id = models.AutoField(primary_key=True)
	recipient_fk = models.ForeignKey(UserProfile, related_name='%(class)srequest_created')
	sender_fk = models.ForeignKey(UserProfile, related_name='%(class)srequest_assigned')
	message_content = models.CharField(max_length=1000, null=True)
	recieved = models.CharField(max_length=1, null=True)
	date_sent = models.DateTimeField(auto_now_add=True, blank=True)	
	city = models.CharField(max_length=80,null=True)
	latitude = models.DecimalField(max_digits=5,decimal_places=2,null=True)
	longitude = models.DecimalField(max_digits=5,decimal_places=2,null=True)
	
class Favor(models.Model):
	favor_id = models.AutoField(primary_key=True)
	title = models.CharField(max_length=30)
	duration = models.IntegerField(null=True)
	description = models.CharField(max_length=100,null=True)
	reward = models.CharField(max_length=100,null=True)
	number_of_participants = models.IntegerField(null=True)
	location_fk = models.ForeignKey(PresetLocation,max_length=20)
	creator_fk = models.ForeignKey(UserProfile)
	deadline = models.DateTimeField()
	date_created = models.DateTimeField(auto_now_add=True, blank=True)
	
class Candidate(models.Model):
	candidate_id = models.AutoField(primary_key=True)
	favor_fk = models.ForeignKey(Favor)
	candidate_fk = models.ForeignKey(UserProfile)
	status = models.CharField(max_length=20)
	rejected = models.CharField(max_length=2)

class Contact(models.Model):
	contact_id = models.AutoField(primary_key=True)
	user_a_fk = models.ForeignKey(UserProfile)
	user_b_fk = models.ForeignKey(UserProfile)
	message_fk = models.ForeignKey(Message,null=True)
	
class FavorImage(models.Model):
	favor_image_id = models.AutoField(primary_key=True)
	favor_fk = models.ForeignKey(Favor)
	image_path = models.CharField(max_length=30,null=True)
	
class LocationDistance(models.Model):
	locationdistance_id = models.AutoField(primary_key=True)
	from_location_fk = models.ForeignKey(PresetLocation,related_name='%(class)sfrom_location')
	to_location_fk = models.ForeignKey(PresetLocation,related_name='%(class)sto_location')
	tier = models.CharField(max_length=3,null=True)
	distance = models.IntegerField(null=True)