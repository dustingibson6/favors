from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'favors_app.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	url(r'^favors/', include('favors.urls')),
	url(r'^admin/', include(admin.site.urls)),
)
