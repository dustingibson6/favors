from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor
import psycopg2, json



class FavorsInstant(Protocol):

	def __init__(self, factory):
		config_text = open("connection.prop",'r').read().replace('\n','')
		self.db_connection = psycopg2.connect(config_text)
		self.factory = factory

	def connectionMade(self):
		self.factory.clients.append(self)
		
	def connectionLost(self, reason):
		self.factory.clients.remove(self)
		
	def execute_query(self,query,results='one'):
		cursor = self.db_connection.cursor()
		cursor.execute(query)
		if results=='one':
			return cursor.fetchall()[0][0]
		else:
			return cursor.fetchall()

	def update_contact(self,new_id,a_id,b_id):
		update_query_a = "update favors_contact set message_fk_id=" + new_id + " where user_a_fk_id=" + a_id + " and user_b_fk_id=" + b_id
		print update_query_a
		cursor = self.db_connection.cursor()
		cursor.execute(update_query_a)
		self.db_connection.commit()
	
	def send_chat(self,recieved_username,sent_username,message):
		recieved_username_query = "SELECT user_profile_id FROM favors_userprofile up WHERE up.username='" + recieved_username + "'"
		sent_username_query = "SELECT user_profile_id FROM favors_userprofile up WHERE up.username='" + sent_username + "'"
		recieved_id = str(self.execute_query(recieved_username_query))
		sent_id = str(self.execute_query(sent_username_query))
		values = recieved_id + "," + sent_id + ",'" + message + "','F',current_date"
		insert_query = "insert into favors_message (recipient_fk_id,sender_fk_id,message_content,recieved,date_sent) values (" + values + ") RETURNING message_id"
		cursor = self.db_connection.cursor()
		cursor.execute(insert_query)
		self.db_connection.commit()
		new_id = str(cursor.fetchone()[0])
		self.update_contact(new_id,sent_id,recieved_id)
		self.update_contact(new_id,recieved_id,sent_id)
		
	def get_chat(self,recieved_username,sent_username):
		json_message = []
		recieved_username_query = "SELECT user_profile_id FROM favors_userprofile up WHERE up.username='" + recieved_username + "'"
		sent_username_query = "SELECT user_profile_id FROM favors_userprofile up WHERE up.username='" + sent_username + "'"
		recieved_id = str(self.execute_query(recieved_username_query))
		sent_id = str(self.execute_query(sent_username_query))
		message_query = "SELECT message_id,message_content,date_sent FROM favors_message WHERE recieved='F' and recipient_fk_id =" + recieved_id + " and sender_fk_id=" + sent_id
		messages_data = self.execute_query(message_query,'multi')
		ids = []
		for message_data in messages_data:
			recieved = {}
			recieved['sender'] = sent_username
			recieved['message'] = message_data[1]
			recieved['date'] = message_data[2].strftime('%B %d, %Y %I:%M %p')
			json_message.append(recieved)
			ids.append(str(message_data[0]))
		if len(ids) > 0:
			update_query = "update favors_message set recieved='T' WHERE message_id in (" + ','.join(ids) + ")"
			print update_query
			cursor = self.db_connection.cursor()
			cursor.execute(update_query)
			self.db_connection.commit()
		return json.dumps(json_message)
			
	def get_message_count(self,recieved_username):
		query = "SELECT count(1) FROM favors_message favmessage JOIN favors_userprofile up on up.user_profile_id = favmessage.recipient_fk_id WHERE up.username='" + recieved_username + "' and recieved='F'"
		cursor = self.db_connection.cursor()
		count = self.execute_query(query)
		print str(count) + "\n"
		return str(count) + "\n"

	def dataReceived(self, data):
		parameters = data.split(',')
		if parameters[0] == "Counts":
			count = self.get_message_count(parameters[1])
			self.factory.clients[0].transport.write(count)
		if parameters[0] == "Send":
			self.send_chat(parameters[1],parameters[2],','.join(parameters[3:]))
			self.factory.clients[0].transport.write("sent\n")
		if parameters[0] == "Get":
			json_message = self.get_chat(parameters[1],parameters[2])
			self.factory.clients[0].transport.write(json_message+"\n")
			
			

class ClientFactory(Factory):
	def __init__(self):
		self.clients = []
		
	def buildProtocol(self, addr):
		return FavorsInstant(self)

reactor.listenTCP(8005, ClientFactory())
reactor.run()


