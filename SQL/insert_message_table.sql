﻿CREATE TABLE "favors_message" (
	"message_id"	INTEGER PRIMARY KEY UNIQUE,
	"recipient_fk_id"	int,
	"sender_fk_id"	int,
	"message_content"	varchar(1000),
	"recieved"	varchar(1)
);

create sequence message_id_seq;
alter table favors_message alter message_id set default nextval('message_id_seq');