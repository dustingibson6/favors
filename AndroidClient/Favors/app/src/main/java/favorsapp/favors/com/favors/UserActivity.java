package favorsapp.favors.com.favors;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import favorsapp.favors.com.favors.sync.GetChatInfoSync;


public class UserActivity extends ActionBarActivity
    implements RegisterFragment.OnFragmentInteractionListener,
    LoginFragment.OnFragmentInteractionListener,
    ChatListFragment.OnFragmentInteractionListener,
    ChatFragment.OnFragmentInteractionListener,
    NewFavorFragment.OnFragmentInteractionListener,
    EditProfileFragment.OnFragmentInteractionListener,
    ViewProfileFragment.OnFragmentInteractionListener,
    DeleteImageFragment.OnFragmentInteractionListener,
    ViewFavorFragment.OnFragmentInteractionListener,
    OnTaskCompleted,
    BeforeTaskExecution {

    private String fragmentToStart;
    private String chatSender;
    private String profileName;
    private String favorID;
    private byte[] favorImageBytes;
    private LoginFragment loginFragment;
    private RegisterFragment registerFragment;
    private ChatListFragment chatListFragment;
    private ChatFragment chatFragment;
    private NewFavorFragment newFavorFragment;
    private EditProfileFragment editProfileFragment;
    private SharedPreferences loginPrefs;
    private ViewProfileFragment viewProfileFragment;
    private DeleteImageFragment deleteImageFragment;
    private ViewFavorFragment favorFragment;
    private OnTaskCompleted onTaskCompletedListener;
    private BeforeTaskExecution beforeTaskExecutionListener;
    private boolean chatCountsThread;
    public Handler countHandler;
    private int chatCount;
    private UserGetChatInfoSync getChatInfoSync;
    public Thread countsThread;

    public UserActivity()
    {
        onTaskCompletedListener = this;
        beforeTaskExecutionListener = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        chatCount = 0;
        fragmentToStart = "";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            fragmentToStart = bundle.getString("fragment","");
            chatSender = bundle.getString("sender","");
            profileName = bundle.getString("profile","");
            favorImageBytes = bundle.getByteArray("delete image");
            favorID = bundle.getString("favorid");
        }
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        loginPrefs = getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        switch(fragmentToStart) {
            case "register":
                registerFragment = new RegisterFragment();
                FragmentTransaction registerTransaction = getSupportFragmentManager().beginTransaction();
                registerTransaction.replace(R.id.user_container, registerFragment);
                //loginTransaction.addToBackStack(null);
                registerTransaction.commit();
                break;
            case "login":
                loginFragment = new LoginFragment();
                FragmentTransaction loginTransaction = getSupportFragmentManager().beginTransaction();
                loginTransaction.replace(R.id.user_container, loginFragment);
                //loginTransaction.addToBackStack(null);
                loginTransaction.commit();
                break;
            case "chatlist":
                chatListFragment = new ChatListFragment();
                FragmentTransaction chatListTransaction = getSupportFragmentManager().beginTransaction();
                chatListTransaction.replace(R.id.user_container,chatListFragment);
                chatListTransaction.commit();
                break;
            case "chat":
                goToChat(chatSender);
                break;
            case "profile":
                editProfileFragment = new EditProfileFragment();
                FragmentTransaction editProfileTransaction = getSupportFragmentManager().beginTransaction();
                editProfileTransaction.replace(R.id.user_container, editProfileFragment);
                editProfileTransaction.commit();
                break;
            case "new favor":
                newFavorFragment = new NewFavorFragment();
                FragmentTransaction newFavorTransaction = getSupportFragmentManager().beginTransaction();
                newFavorTransaction.replace(R.id.user_container, newFavorFragment);
                newFavorTransaction.commit();
                break;
            case "favor":
                goToFavor();
                break;
            case "view profile":
                goToProfile(profileName);
                break;
            default:
                break;
        }


        //buildChatWindow();
    }

    public void startCountsThread() {
        countHandler = new Handler();
        chatCountsThread = true;
        countsThread =
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(chatCountsThread) {
                            try {
                                Thread.sleep(2000);
                                countHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        String sessionKey = loginPrefs.getString("sessionKey", null);
                                        if(sessionKey != null) {
                                            //getChatInfoSync = new UserGetChatInfoSync(loginPrefs);
                                            //getChatInfoSync.execute();
                                        }
                                        //if(Thread.interrupted())
                                            //getChatInfoSync.cancel(true);
                                    }
                                });
                            } catch(Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }}
                );
        countsThread.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        startCountsThread();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            chatCountsThread = false;
            countsThread.interrupt();
            countsThread.join();
            countsThread = null;
        } catch( InterruptedException ie ) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user, menu);
        setProfileName(menu);
        setChatCount(menu);
        return true;
    }

    public void switchToLogin() {
        loginFragment = new LoginFragment();
        FragmentTransaction loginTransaction = getSupportFragmentManager().beginTransaction();
        loginTransaction.replace(R.id.user_container, loginFragment);
        //loginTransaction.addToBackStack(null);
        loginTransaction.commit();
    }

    public void setChatCount(Menu menu) {
        MenuItem chatCountButtonItem = menu.findItem(R.id.action_user_chat_count);
        String displayCount = String.valueOf(chatCount);
        chatCountButtonItem.setTitle(displayCount);
    }

    public void setProfileName(Menu menu) {
        MenuItem profileButtonItem = menu.findItem(R.id.action_profile);
        String displayUserName = loginPrefs.getString("username", null);
        //TODO: User profile image instead of name text
        if (displayUserName == null) {
            displayUserName = "Guest";
        }
        profileButtonItem.setTitle(displayUserName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToChat(String sender) {
        chatFragment = new ChatFragment();
        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putString("sender",sender);
        chatFragment.setArguments(fragmentBundle);
        FragmentTransaction chatTransaction = getSupportFragmentManager().beginTransaction();
        chatTransaction.replace(R.id.user_container, chatFragment);
        //chatTransaction.addToBackStack(null);
        chatTransaction.commit();
    }

    public void goToProfile(String name) {
        viewProfileFragment = new ViewProfileFragment();
        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putString("profile",name);
        viewProfileFragment.setArguments(fragmentBundle);
        FragmentTransaction viewProfileTransaction = getSupportFragmentManager().beginTransaction();
        viewProfileTransaction.replace(R.id.user_container, viewProfileFragment);
        viewProfileTransaction.commit();
    }

    public void goToFavor() {
        favorFragment = new ViewFavorFragment();
        Bundle favorFragmentBundle = new Bundle();
        favorFragmentBundle.putString("favorid",favorID);
        favorFragment.setArguments(favorFragmentBundle);
        FragmentTransaction favorFragmentTransaction = getSupportFragmentManager().beginTransaction();
        favorFragmentTransaction.replace(R.id.user_container, favorFragment);
        favorFragmentTransaction.commit();
    }

    public void goToDeleteImage(byte[] imageBytes, int index) {
        deleteImageFragment = new DeleteImageFragment();
        Bundle deleteFragmentBundle = new Bundle();
        deleteFragmentBundle.putByteArray("delete image", imageBytes);
        deleteFragmentBundle.putInt("index", index);
        deleteImageFragment.setArguments(deleteFragmentBundle);
        FragmentTransaction deleteImageTransaction = getSupportFragmentManager().beginTransaction();
        deleteImageTransaction.replace(R.id.user_container, deleteImageFragment);
        deleteImageTransaction.addToBackStack(null);
        //deleteImageTransaction.add(viewProfileFragment,"new favors");
        deleteImageTransaction.commit();
    }

    public void onFragmentInteraction(Uri uri) {

    }

    void refreshActionBar(String status) {
        try {
            chatCount = Integer.parseInt(status);
            this.invalidateOptionsMenu();
        } catch(Exception e) {

        }
    }

    public class UserGetChatInfoSync extends GetChatInfoSync {

        public UserGetChatInfoSync(SharedPreferences loginPrefs) {
            super(beforeTaskExecutionListener,onTaskCompletedListener);
        }

        @Override
        protected void onPostExecute(String result) {
            refreshActionBar(status);
        }

    }

    public void PrepareExecution(String event) {

    }

    public void UpdateUI(String status, String event) {

    }

    public void SetAutoComplete(String[] stringArray) {

    }

}
