package favorsapp.favors.com.favors;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import favorsapp.favors.com.favors.sync.AddContactSync;
import favorsapp.favors.com.favors.sync.GetFavorSync;
import favorsapp.favors.com.favors.sync.SetCandidateSync;

public class ViewFavorFragment extends Fragment implements BeforeTaskExecution, OnTaskCompleted {

    protected String sessionKey;
    protected SharedPreferences loginPrefs;
    protected String selectedFavorID;
    protected TextView titleText;
    protected TextView descriptionText;
    protected TextView rewardText;
    protected TextView timeLeftText;
    protected TextView authorText;
    protected ImageView displayImage;
    protected ImageButton nextButton;
    protected ImageButton prevButton;
    protected Button interestedButton;
    protected Button notInterestedButton;
    protected ImageButton chatButton;
    protected GetFavorSync favorSync;
    protected ProgressBar progressBar;
    protected LinearLayout progressLayout;
    protected SetCandidateSync candidateSync;
    protected AddContactSync addContactSync;
    protected ArrayList<Bitmap> images;
    protected int currentIndex;
    protected String zip;
    private ObjectAnimator progressAnimator;
    private OnTaskCompleted onTaskCompletedListener;
    private BeforeTaskExecution beforeTaskExecution;
    private String authorUsername;

    class FavorInfo {
        public String title;
        public String description;
        public String reward;

        public FavorInfo(String title, String description, String reward) {
            this.title = title;
            this.description = description;
            this.reward =  reward;
        }
    }

    public static ViewFavorFragment newInstance() {
        ViewFavorFragment fragment = new ViewFavorFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ViewFavorFragment() {
        beforeTaskExecution = this;
        onTaskCompletedListener = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        selectedFavorID = getArguments().getString("favorid");
        super.onCreate(savedInstanceState);
    }

    void RefreshFragment() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.detach(this).attach(this).commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        images = new ArrayList<Bitmap>();
        currentIndex = 0;
        View view = inflater.inflate(R.layout.fragment_view_favor, container, false);
        titleText = (TextView) view.findViewById(R.id.viewFavorTitle);
        descriptionText = (TextView) view.findViewById(R.id.viewFavorDescriptionText);
        rewardText = (TextView) view.findViewById(R.id.viewFavorRewardsText);
        timeLeftText = (TextView) view.findViewById(R.id.viewFavorDeadlineText);
        displayImage = (ImageView) view.findViewById(R.id.viewFavorImage);
        authorText = (TextView) view.findViewById(R.id.viewFavorAuthor);
        prevButton = (ImageButton) view.findViewById(R.id.viewFavorPrevImage);
        nextButton = (ImageButton) view.findViewById(R.id.viewFavorNextImage);
        interestedButton = (Button) view.findViewById(R.id.viewFavorInterested);
        notInterestedButton = (Button) view.findViewById(R.id.viewFavorNotInterested);
        chatButton = (ImageButton) view.findViewById(R.id.viewFavorChat);
        progressLayout = (LinearLayout) view.findViewById(R.id.viewFavorLayoutProgress);
        loginPrefs = getActivity().getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        progressBar = (ProgressBar) view.findViewById(R.id.viewFavorProgress);
        progressAnimator = ObjectAnimator.ofInt(progressBar, "progress",0,100);
        progressAnimator.setDuration(1000);
        sessionKey = loginPrefs.getString("sessionKey", null);
        prevButton.setOnClickListener(
               new Button.OnClickListener() {
                   public void onClick(View view) {
                       currentIndex = currentIndex - 1;
                       setImage();
                   }
               }
        );
        nextButton.setOnClickListener(
            new Button.OnClickListener() {
                public void onClick(View view) {
                    currentIndex = currentIndex + 1;
                    setImage();
                }
            }
        );
        interestedButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        candidateSync = new SetCandidateSync(beforeTaskExecution,onTaskCompletedListener);
                        candidateSync.execute(sessionKey, selectedFavorID, "Interested");
                    }
                });
        notInterestedButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        candidateSync = new SetCandidateSync(beforeTaskExecution,onTaskCompletedListener);
                        candidateSync.execute(sessionKey, selectedFavorID, "Not Interested");
                    }
                });
        chatButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        addContactSync = new AddContactSync(onTaskCompletedListener);
                        addContactSync.execute(sessionKey,authorUsername);
                    }
                });
        zip = loginPrefs.getString("zip",null);
        favorSync = new GetFavorSync(beforeTaskExecution,onTaskCompletedListener);
        favorSync.execute(sessionKey,zip,selectedFavorID);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    public void setImage() {
        prevButton.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.INVISIBLE);
        if( images.size() > 0) {
            displayImage.setImageBitmap(images.get(currentIndex));
            if (currentIndex == 0 && images.size() > 1) {
                prevButton.setVisibility(View.INVISIBLE);
                nextButton.setVisibility(View.VISIBLE);
            }
            else if (currentIndex == images.size() - 1 && images.size() > 1) {
                nextButton.setVisibility(View.INVISIBLE);
                prevButton.setVisibility(View.VISIBLE);
            }
            else if(images.size() > 1 ) {
                prevButton.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.VISIBLE);
            }
        }
    }

    public void SetText(String jsonString) {
        if(jsonString.equals("fail"))
            return;
        try {
            JSONArray favorArray = new JSONArray(jsonString);
            JSONObject currentObject = favorArray.getJSONObject(0);
            String title = currentObject.getString("title");
            String description = currentObject.getString("description");
            String reward = currentObject.getString("reward");
            String deadline = currentObject.getString("deadline");
            authorUsername = currentObject.getString("author_username");
            String authorFirstName = currentObject.getString("author_firstname");
            String authorLastName = currentObject.getString("author_lastname");
            String status = currentObject.getString("status");
            String displayName = "Created By: " + FavorsGlobal.getInstance().getDisplayName(authorUsername,authorFirstName,authorLastName);
            DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a");
            DateTime deadlineDateTime = formatter.parseDateTime(deadline);
            DateTime currentDateTime = DateTime.now();
            JSONArray imageArray = currentObject.getJSONArray("images");
            String timeLeft = FavorsGlobal.getInstance().GetTimeLeftString(currentDateTime,deadlineDateTime);
            titleText.setText(title);
            for(int i = 0; i < imageArray.length(); i++) {
                JSONObject currentImageObject = imageArray.getJSONObject(i);
                String currentPath = currentImageObject.getString("image");
                images.add(FavorsGlobal.getInstance().GetBitmapFromWeb(currentPath));
            }
            setImage();
            if(status.equals("Interested")) {
                Drawable activeInterested = ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_interested_active);
                interestedButton.setCompoundDrawablesWithIntrinsicBounds(activeInterested, null, null, null);
                interestedButton.setTextColor(Color.GREEN);
            }
            else if(status.equals("Not Interested")) {
                Drawable activeNotInterested = ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_notinterested_active);
                notInterestedButton.setCompoundDrawablesWithIntrinsicBounds(activeNotInterested,null,null,null);
                notInterestedButton.setTextColor(Color.RED);
            }
            timeLeftText.setText(timeLeft);
            descriptionText.setText(description);
            rewardText.setText(reward);
            authorText.setText(displayName);
        } catch(JSONException jsonE) {
            jsonE.printStackTrace();
        }
    }

    public void SetAutoComplete(String[] stringArray)
    {

    }

    public void UpdateUI(String status, String event)
    {
        if(event.equals("Get Favor")) {
            SetText(status);
            progressLayout.setVisibility(View.GONE);
        }
        else if(event.equals("Set Contact")) {
            ((UserActivity)getActivity()).goToChat(authorUsername);
        }
        else if(event.equals("Set Candidate")) {
            getActivity().onBackPressed();
        }
    }

    public void PrepareExecution(String event) {
        if(event.equals("Get Favor")) {
            progressLayout.setVisibility(View.VISIBLE);
            progressAnimator.start();
        }
    }

}
