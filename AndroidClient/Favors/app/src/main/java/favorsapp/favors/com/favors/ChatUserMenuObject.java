package favorsapp.favors.com.favors;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChatUserMenuObject {

    private List<MenuChatItemInfo> chatMenuItems;

    public ChatUserMenuObject() {
        chatMenuItems = new ArrayList<MenuChatItemInfo>();

    }

    public String getUserString(MenuItem item) {
        for (int i = 0; i < chatMenuItems.size(); i++) {
            if (item.getItemId() == chatMenuItems.get(i).menuItem.getItemId()) {
                return chatMenuItems.get(i).username;
            }
        }
        return null;
    }

    public PopupMenu buildChatMenu(Context context, String jsonString, View popUpMenu ,SharedPreferences loginPrefs) {
        List<String> displayChatNames = new ArrayList<String>();
        int JSONsize = 0;
        PopupMenu chatPopup = new PopupMenu(context, popUpMenu);
        try {
            JSONArray chatUsersJSON = new JSONArray(jsonString);
            //JSONsize = chatUsersJSON.length();
            for(int i=0; i < chatUsersJSON.length(); i++) {
                JSONObject currentObject = chatUsersJSON.getJSONObject(i);
                String username = currentObject.getString("username");
                String firstName = currentObject.getString("firstname");
                String lastName = currentObject.getString("lastname");
                String displayName = FavorsGlobal.getInstance().getDisplayName(username, firstName, lastName);
                MenuChatItemInfo menuChatItemInfo = new MenuChatItemInfo(username,chatPopup.getMenu().add(displayName));
                chatMenuItems.add(i,menuChatItemInfo);
            }
        } catch(JSONException jsonE) {
            jsonE.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        }
        //chatPopup.setOnMenuItemClickListener(this);
        return chatPopup;
    }

        public class MenuChatItemInfo {
            public String username;
            public MenuItem menuItem;

            public MenuChatItemInfo(String username, MenuItem menuItem) {
                this.username = username;
                this.menuItem = menuItem;
            }
        }

}
