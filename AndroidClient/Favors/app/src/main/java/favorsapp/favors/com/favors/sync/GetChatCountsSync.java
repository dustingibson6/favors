package favorsapp.favors.com.favors.sync;


import android.os.AsyncTask;

import favorsapp.favors.com.favors.OnTaskCompleted;
import favorsapp.favors.com.favors.TCPConnect;

public class GetChatCountsSync extends AsyncTask<String,String,String> {
    public int chatCount;
    protected String status;
    protected OnTaskCompleted onTaskCompletedListener;

    public GetChatCountsSync(OnTaskCompleted onTaskCompletedListener) {
        chatCount = 0;
        this.onTaskCompletedListener = onTaskCompletedListener;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(String... params) {
        String username =  params[0];
        String status = TCPConnect.getInstance().getMessageCounts(username);
        if(status.equals("ERROR"))
            chatCount = 0;
        else
            chatCount = Integer.parseInt(status);
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        onTaskCompletedListener.UpdateUI(String.valueOf(chatCount),"Get Chat Count");
    }
}