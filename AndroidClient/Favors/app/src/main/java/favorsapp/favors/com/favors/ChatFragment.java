package favorsapp.favors.com.favors;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChatFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private List<ListInfo> listInfo;
    private List<String> rowList;
    private SharedPreferences loginPrefs;
    private SharedPreferences chatPrefs;
    private SendChatMessageSync sendChatMessageSync;
    private boolean chatThreadRun;
    public EditText messageEdit;
    public String sessionKey;
    public static String sender;
    public ListView chatMessageListView;
    public ChatMessageListAdapter chatMessageListAdapter;
    public GetChatMessageSync getChatMessageSync;
    public Handler countHandler;
    public String postMessage;
    public Thread chatThread;

    public static ChatFragment newInstance() {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ChatFragment() {
        listInfo = new ArrayList<ListInfo>();
        postMessage = "";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //sender = args.getString("sender");
        if (getArguments() != null) {
        }
    }



    public void saveChat() {
        chatPrefs = getActivity().getSharedPreferences("chatPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor chatEditor = chatPrefs.edit();
        Gson chatGson = new Gson();
        ListInfoObject listInfoObject = new ListInfoObject(listInfo);
        String json = chatGson.toJson(listInfoObject);
        chatEditor.putString(sender + "_chat", json);
        chatEditor.commit();
    }

    public void goToProfile(String name) {
        ((UserActivity)this.getActivity()).goToProfile(name);
    }

    public void loadChat() {
        try {
            chatPrefs = getActivity().getSharedPreferences("chatPrefs", Context.MODE_PRIVATE);
            Gson gson = new Gson();
            String json = chatPrefs.getString(sender + "_chat", "");
            ListInfoObject listInfoObject = gson.fromJson(json, ListInfoObject.class);
            listInfo = listInfoObject.listInfo;
            rowList = createRowList(listInfo);
            chatMessageListAdapter = new ChatMessageListAdapter(this.getActivity(), R.layout.chat_list_layout, listInfo, rowList);
            chatMessageListView.setAdapter(chatMessageListAdapter);
        } catch(Exception e) {
            System.out.println("Does not exist");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_chat, container, false);
        messageEdit = (EditText) fragmentView.findViewById(R.id.chatEditText);
        sender = getArguments().getString("sender");
        loginPrefs = getActivity().getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        sessionKey = loginPrefs.getString("sessionKey", null);
        //sender = loginPrefs.getString("username", null);
        chatMessageListView = (ListView) fragmentView.findViewById(R.id.chatMessageListView);
        getChatMessageSync = new GetChatMessageSync();
        getChatMessageSync.execute();
        Button sendButton = (Button) fragmentView.findViewById(R.id.sendChatButton);
        ImageButton profileButton = (ImageButton) fragmentView.findViewById(R.id.chatProfileImageButton);
        sendButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        sendMessage();
                    }
                });
        profileButton.setOnClickListener(
                new ImageButton.OnClickListener() {
                    public void onClick(View view) {
                        goToProfile(sender);
                    }
                });
        countHandler = new Handler();
        //buildChatWindow();

        loadChat();
        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        startChatThread();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            chatThreadRun = false;
            chatThread.interrupt();
            chatThread.join();
            if(getChatMessageSync != null)
                getChatMessageSync.cancel(true);
            chatThread = null;
        } catch(InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    public void startChatThread() {
        chatThreadRun = true;
        chatThread =
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(chatThreadRun) {
                            try {
                                countHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(Thread.interrupted()) {
                                            getChatMessageSync.cancel(true);
                                            return;
                                        }
                                String sessionKey = loginPrefs.getString("sessionKey", null);
                                if(sessionKey != null  && Thread.interrupted() == false) {
                                    getChatMessageSync = new GetChatMessageSync();
                                    getChatMessageSync.execute();
                                }
                                    }
                                });
                                Thread.sleep(1000);
                            } catch(Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }}
                );
        chatThread.start();
    }

    public void sendMessage() {
        postMessage = messageEdit.getText().toString();
        if(chatMessageListAdapter == null) {
            rowList = createRowList(listInfo);
            chatMessageListAdapter = new ChatMessageListAdapter(this.getActivity(), R.layout.chat_list_layout, listInfo, rowList);
            chatMessageListView.setAdapter(chatMessageListAdapter);
        }
        String yourName = "Me";
        if(loginPrefs.getString("username",null) != null)
            yourName = loginPrefs.getString("username",null);
        ListInfo message = new ListInfo(yourName,messageEdit.getText().toString());
        listInfo.add(message);
        rowList = createRowList(listInfo);
        chatMessageListAdapter.add(yourName);
        sendChatMessageSync = new SendChatMessageSync();
        sendChatMessageSync.execute();
        chatMessageListAdapter.listMap.put(listInfo.size()-1,message);
        chatMessageListAdapter.notifyDataSetChanged();
        saveChat();
        messageEdit.setText("");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public List<String> createRowList(List<ListInfo> chatList) {
        List<String> rowList = new ArrayList<String>();
        for(int i = 0; i < chatList.size(); i++) {
            rowList.add(chatList.get(i).message);
        }
        return rowList;
    }

    public void refreshList(String jsonString) {
        int JSONsize = 0;
        try {
            JSONArray chatListMessageJSON = new JSONArray(jsonString);
            JSONsize = chatListMessageJSON.length();
            for(int i=0; i < chatListMessageJSON.length(); i++) {
                JSONObject currentObject = chatListMessageJSON.getJSONObject(i);
                String message = currentObject.getString("message");
                String sender = currentObject.getString("sender");
                ListInfo newInfo = new ListInfo(sender,message);
                listInfo.add(listInfo.size(),newInfo);
            }
        } catch(JSONException jsonE) {
            jsonE.printStackTrace();
            return;
        }
        if(JSONsize > 0) {
            rowList = createRowList(listInfo);
            chatMessageListAdapter = new ChatMessageListAdapter(this.getActivity(), R.layout.chat_list_layout, listInfo, rowList);
            chatMessageListView.setAdapter(chatMessageListAdapter);
        }
        TextView chatNameTextView = (TextView)this.getActivity().findViewById(R.id.chatNameTextView);
        chatNameTextView.setText(sender);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public class ChatMessageListAdapter extends ArrayAdapter<String> {
        HashMap<Integer, ListInfo> listMap = new HashMap<Integer,ListInfo>();
        List<String> rowList;
        private Activity context;

        public ChatMessageListAdapter(Activity context, int resourceId, List<ListInfo> objects, List<String> rowList) {
            super(context, resourceId, rowList);
            this.context = context;
            for(int i = 0; i < objects.size(); i++)
                listMap.put(i,objects.get(i));
            this.rowList = rowList;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(convertView == null) {
                LayoutInflater inflator = context.getLayoutInflater();
                view = inflator.inflate(R.layout.chat_layout,null);
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.userLabel = (TextView) view.findViewById(R.id.senderTextView);
                viewHolder.messageLabel = (TextView) view.findViewById(R.id.messageTextView);
                viewHolder.userLabel.setTypeface(null, Typeface.BOLD);
                viewHolder.userLabel.setTextSize(16);
                view.setTag(viewHolder);
            }
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            ListInfo currentInfo = listMap.get(position);
            String userString = currentInfo.sender;
            String messageString = currentInfo.message;
            viewHolder.userLabel.setText(userString);
            viewHolder.messageLabel.setText(messageString);
            if(userString.equals(sender)) {
                viewHolder.userLabel.setTextColor(ColorStateList.valueOf(Color.RED));
            }
            else
                viewHolder.userLabel.setTextColor(ColorStateList.valueOf(Color.BLUE));
            return view;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }

    public class GetChatMessageSync extends AsyncTask<String,String,String> {
        protected String status;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            String username =  loginPrefs.getString("username",null);
            status = TCPConnect.getInstance().getChatMessage(username,sender);
            return status;
        }

        @Override
        protected void onPostExecute(String result) {
            refreshList(status);
        }
    }


    public class SendChatMessageSync extends AsyncTask<String,String,String> {
        protected String status;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            String message = messageEdit.getText().toString();
            String username =  loginPrefs.getString("username", null);
            String status = TCPConnect.getInstance().sendChatMessage(sender, username, message);
            return status;
        }

        @Override
        protected void onPostExecute(String result) {

        }
    }

    static class ViewHolder {
        TextView userLabel;
        TextView messageLabel;
    }

    public class ListInfo {

        public String message;
        public String sender;

        ListInfo(String sender, String message) {
            this.message = message;
            this.sender = sender;
        }
    }

    public class ListInfoObject {
        public List<ListInfo> listInfo;
        public ListInfoObject(List<ListInfo> listInfo) {
            this.listInfo = listInfo;
        }
    }

}
