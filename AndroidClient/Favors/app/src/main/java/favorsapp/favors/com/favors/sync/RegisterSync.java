package favorsapp.favors.com.favors.sync;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import favorsapp.favors.com.favors.OnTaskCompleted;

public class RegisterSync extends AsyncTask<String,String,String> {

    protected String status;
    private OnTaskCompleted onTaskCompleted;

    public RegisterSync(OnTaskCompleted onTaskCompleted) {
        this.onTaskCompleted = onTaskCompleted;
    }

    @Override
    protected void onPreExecute() {
        //holdOrientationControl();
    }

    @Override
    protected String doInBackground(String... params) {
        status = "fail";
        try {
            URL registerURL = new URL("http://45.55.133.160/favors/registration.html");
            HttpURLConnection connection = (HttpURLConnection) registerURL.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //connection.setRequestProperty("Content-Length", ""+Integer.toString(urlParameters.getBytes().length));
            OutputStreamWriter outWriter = new OutputStreamWriter ( connection.getOutputStream());
            String modZip = params[3].toString().replace("(","").replace(")","").trim();
            modZip = modZip.substring(modZip.length()-5,modZip.length());
            String postString = "";
            postString += "username=" + URLEncoder.encode(params[0]) + "&";
            postString += "password=" + URLEncoder.encode(params[1]) + "&";
            postString += "email=" + URLEncoder.encode(params[2]) + "&";
            postString += "zipcode=" + URLEncoder.encode(modZip);
            outWriter.write(postString);
            outWriter.close();
            if( connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                //String content = org.apache.
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while(((line = br.readLine()) != null)) {
                    status = line;
                    return line;
                }
            } else {
                return "fail";
            }
        } catch (MalformedURLException eURL) {
            return "fail";
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
        return "fail";
    }

    @Override
    protected void onProgressUpdate(String... values) {

    }

    @Override
    protected void onPostExecute(String result) {
        onTaskCompleted.UpdateUI(status,"");
        //TODO: Return to home screen
        //releaseOrientationControl();
        //getRegistrationStatus(status);
    }
}
