package favorsapp.favors.com.favors;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class DeleteImageFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    private Bitmap image;
    private ImageView previewImage;
    private Button deleteButton;
    private Button cancelButton;
    private int index;

    public static DeleteImageFragment newInstance() {
        DeleteImageFragment fragment = new DeleteImageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public DeleteImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        byte[] favorImageBytes =  getArguments().getByteArray("delete image");
        index = getArguments().getInt("index");
        image = BitmapFactory.decodeByteArray(favorImageBytes, 0, favorImageBytes.length);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentMessages fragmentMessages = FragmentMessages.getInstance();
        fragmentMessages.setDeleteIndex(-1);
        View fragmentView =  inflater.inflate(R.layout.fragment_delete_image, container, false);
        previewImage = (ImageView)fragmentView.findViewById(R.id.deleteImageView);
        deleteButton = (Button)fragmentView.findViewById(R.id.deleteImageDeleteButton);
        cancelButton = (Button)fragmentView.findViewById(R.id.deleteImageCancelButton);
        deleteButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        FragmentMessages fragmentMessages = FragmentMessages.getInstance();
                        fragmentMessages.setDeleteIndex(index);
                        getFragmentManager().popBackStack();
                    }
                }
        );
        cancelButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        getFragmentManager().popBackStack();
                    }
                }
        );
        Bitmap currentImage = image;
        int height = (int) ( currentImage.getHeight() * (512.0 / currentImage.getWidth()) );
        Bitmap newImage = Bitmap.createScaledBitmap(currentImage, 512, height, true);
        previewImage.setImageBitmap(newImage);
        return fragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
