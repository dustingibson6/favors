package favorsapp.favors.com.favors.sync;


import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import favorsapp.favors.com.favors.BeforeTaskExecution;
import favorsapp.favors.com.favors.OnTaskCompleted;

public class GetChatInfoSync extends AsyncTask<String,String,String> {
    protected String status;
    private OnTaskCompleted onTaskCompletedListener;
    private BeforeTaskExecution beforeTaskExecutionListener;

    public GetChatInfoSync(BeforeTaskExecution beforeTaskExecutionListener, OnTaskCompleted onTaskCompletedLissterner)
    {
        this.onTaskCompletedListener = onTaskCompletedLissterner;
        this.beforeTaskExecutionListener = beforeTaskExecutionListener;
    }

    @Override
    protected void onPreExecute() {
        beforeTaskExecutionListener.PrepareExecution("");
    }

    @Override
    protected String doInBackground(String... params) {
        String sessionKey = params[0];
        status = "fail";
        if(sessionKey == null) {
            status = "Logged Out";
            return "fail";
        }
        try {
            URL userInfoURL = new URL("http://45.55.133.160/favors/get_chat_list.html");
            HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Cookie", "sessionid=" + sessionKey);
            int responseCode = connection.getResponseCode();
            BufferedReader inReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuffer response = new StringBuffer();
            status = "";
            while ((line = inReader.readLine()) != null) {
                status += line;
            }
            connection.disconnect();
            return status;
        } catch (MalformedURLException eURL) {
            return "fail";
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if(!this.isCancelled()) {
            onTaskCompletedListener.UpdateUI(status,"");
        }
    }
}