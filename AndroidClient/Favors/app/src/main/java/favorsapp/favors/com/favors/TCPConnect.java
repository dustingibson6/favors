package favorsapp.favors.com.favors;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class TCPConnect {

    private static TCPConnect instance = null;
    private boolean reading;
    private Socket clientSocket;
    private DataOutputStream outServer;
    private BufferedReader inServer;
    private String counts;

    private TCPConnect() {
        this.reading = false;
        try {
            clientSocket = new Socket("45.55.133.160", 8005);
            outServer = new DataOutputStream((clientSocket.getOutputStream()));
            inServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (Exception ie) {
            System.out.println("Error");
        }
    }

    public static TCPConnect getInstance() {
        if(instance == null) {
            instance = new TCPConnect();
        }
        return instance;
    }

    public String getMessageCounts(String username) {
        String status = "";
        try {
            String sent_data = "Counts," + username;
            outServer.writeBytes(sent_data);
            status = inServer.readLine();
            outServer.flush();
            return status;
        } catch (Exception ie) {
            System.out.println("ERROR");
            return status;
        }
    }

    public String getChatMessage(String recieved_username, String sent_username) {
        String status = "";
        try {
            String sent_data = "Get," + recieved_username + "," + sent_username;
            outServer.writeBytes(sent_data);
            status = inServer.readLine();
            outServer.flush();
            return status;
        } catch (Exception ie) {
            System.out.println("ERROR");
            return status;
        }
    }

    public String sendChatMessage(String recieved_username, String sent_username, String message) {
        String status = "";
        try {
            String sent_data = "Send," + recieved_username + "," + sent_username + "," + message + "\n";
            outServer.writeBytes(sent_data);
            status = inServer.readLine();
            outServer.flush();
            return status;
        } catch (Exception ie) {
            System.out.println("ERROR");
            return status;
        }
    }

}
