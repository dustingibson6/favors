package favorsapp.favors.com.favors.sync;

import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import favorsapp.favors.com.favors.OnTaskCompleted;

public class LoginSync extends AsyncTask<String,String,String> {

    private String status;
    private OnTaskCompleted onTaskCompletedListener;

    public LoginSync(OnTaskCompleted onTaskCompletedListener) {
        this.onTaskCompletedListener = onTaskCompletedListener;
    }

    @Override
    protected void onPreExecute() {
        //holdOrientationControl();
    }

    @Override
    protected String doInBackground(String... params) {
        status = "fail";
        try {
            URL loginURL = new URL("http://45.55.133.160/favors/login.html");
            HttpURLConnection connection = (HttpURLConnection) loginURL.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            OutputStreamWriter outWriter = new OutputStreamWriter ( connection.getOutputStream());
            String postString = "";
            postString += "username=" + URLEncoder.encode(params[0]) + "&";
            postString += "password=" + URLEncoder.encode(params[1]);
            outWriter.write(postString);
            outWriter.close();
            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while (((line = br.readLine()) != null)) {
                    status = line;
                    return line;
                }
            }
            return "fail";
        } catch (MalformedURLException eURL) {
            return "fail";
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        onTaskCompletedListener.UpdateUI(result,"");
    }

}