package favorsapp.favors.com.favors.sync;

import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import favorsapp.favors.com.favors.OnTaskCompleted;

public class GetAutoLocationSync extends AsyncTask<String,String,String> {
    public String array;
    private OnTaskCompleted onTaskCompleted;
    protected AutoCompleteTextView autoLocationText;
    protected ArrayAdapter autoAdapter;
    protected String status;

    public GetAutoLocationSync(OnTaskCompleted listener) {
        onTaskCompleted = listener;
    }

    public void setAutoText(String jsonString) {
        try {
            ArrayList<String> stringList = new ArrayList<String>();
            JSONArray autoLocationJSON = new JSONArray(jsonString);
            int JSONsize = autoLocationJSON.length();
            for(int i=0; i < autoLocationJSON.length(); i++) {
                JSONObject currentObject = autoLocationJSON.getJSONObject(i);
                String zipObject = currentObject.getString("location");
                stringList.add(zipObject);
            }
            String[] stringArray = stringList.toArray(new String[0]);
            onTaskCompleted.SetAutoComplete(stringArray);
        } catch(JSONException jsonE) {
            jsonE.printStackTrace();
            return;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        //progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... params) {
        status = "fail";
        try {
            URL userInfoURL = new URL("http://45.55.133.160/favors/auto_location.html");
            HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Cookie", "location=" + params[0]);
            int responseCode = connection.getResponseCode();
            BufferedReader inReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = inReader.readLine()) != null) {
                status = line;
                return line;
            }
            connection.disconnect();
            return "fail";
        } catch (MalformedURLException eURL) {
            return "fail";
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        setAutoText(status);
    }
}