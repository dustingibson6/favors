package favorsapp.favors.com.favors.sync;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import favorsapp.favors.com.favors.BeforeTaskExecution;
import favorsapp.favors.com.favors.OnTaskCompleted;

public class SetProfileSync extends AsyncTask<String,String,String> {
    protected String status;

    protected BeforeTaskExecution beforeTaskExecutionListener;
    protected OnTaskCompleted onTaskCompletedListener;
    protected String firstName;
    protected String lastName;
    protected String sessionKey;
    protected String qualifications;
    protected String zip;
    private Bitmap profileImage;
    private UploadImageSync uploadImageSync;


    public SetProfileSync(BeforeTaskExecution beforeTaskExecution, OnTaskCompleted onTaskCompletedListener, Bitmap profileImage)
    {
        this.beforeTaskExecutionListener = beforeTaskExecution;
        this.onTaskCompletedListener = onTaskCompletedListener;
        this.profileImage = profileImage;
    }

    @Override
    protected void onPreExecute() {
        beforeTaskExecutionListener.PrepareExecution("Set Profile");
    }

    @Override
    protected void onCancelled() {
        if(uploadImageSync != null)
            uploadImageSync.cancel(true);
    }

    @Override
    protected String doInBackground(String... params) {
        status = "fail";
        sessionKey = params[0];
        firstName = params[1];
        lastName = params[2];
        qualifications = params[3];
        String mod_zip = params[4].toString().replace("(","").replace(")","").trim();
        zip  = mod_zip.substring(mod_zip.length()-5,mod_zip.length());
        try {
            URL userInfoURL = new URL("http://45.55.133.160/favors/profile.html");
            HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
            //connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Cookie","sessionid=" + sessionKey);
            OutputStreamWriter outWriter = new OutputStreamWriter(connection.getOutputStream());
            String postString = "";
            postString += "firstName=" + firstName + "&";
            postString += "lastName=" + lastName + "&";
            postString += "zip=" + zip + "&";
            postString += "qualifications=" + qualifications;
            outWriter.write(postString);
            outWriter.close();
            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader inReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = inReader.readLine()) != null) {
                    status = line;
                    System.out.println(line);
                    return line;
                }
            }
            return "fail";
        } catch (MalformedURLException eURL) {
            return "fail";
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if(profileImage == null)
            onTaskCompletedListener.UpdateUI(status,"Set Profile");
        else {
            uploadImageSync = new UploadImageSync();
            uploadImageSync.execute();
        }
    }


    public class UploadImageSync extends AsyncTask<String,String,String> {
        protected String status;


        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            String boundary = "*****";
            status = "fail";
            try {
                URL userInfoURL = new URL("http://45.55.133.160/favors/upload_profile_photo.html");
                HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
                connection.setUseCaches(false);
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection","Keep-Alive");
                connection.setRequestProperty("Cache-Control","no-cache");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
                connection.setRequestProperty("Cookie","sessionid=" + sessionKey);
                DataOutputStream request = new DataOutputStream(connection.getOutputStream());
                request.writeBytes("--" + boundary + "\r\n");
                request.writeBytes("Content-Disposition: form-data; name=\"photoFile\"; filename=\"test.bmp\"\r\n");
                //request.writeBytes("Content-Type: Image/png\r\n");
                request.writeBytes("\r\n");
                //Conversion @ http://stackoverflow.com/questions/4989182/converting-java-bitmap-to-byte-array

                ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                if(profileImage != null && !profileImage.compress(Bitmap.CompressFormat.PNG, 100, byteStream)) {
                    status = "Invalid Image";
                    return "fail";
                }
                //Limit to 5 MB
                byte[] byteArray = byteStream.toByteArray();
                if(byteArray.length > 150000000) {
                    status = "Image is too large";
                    return "fail";
                }
                request.write(byteArray);
                request.writeBytes("\r\n");
                request.writeBytes("--" + boundary + "--" + "\r\n");
                request.flush();
                request.close();
                int responseCode = connection.getResponseCode();
                BufferedReader inReader = new BufferedReader( new InputStreamReader(connection.getInputStream()));
                String line;
                StringBuffer response = new StringBuffer();
                while((line = inReader.readLine()) != null) {
                    status = line;
                    System.out.println(line);
                    return line;
                }
                return "fail";
            } catch (MalformedURLException eURL) {
                return "fail";
            } catch (IOException e) {
                e.printStackTrace();
                return "fail";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            onTaskCompletedListener.UpdateUI(status, "Set Profile");
        }

    }
}
