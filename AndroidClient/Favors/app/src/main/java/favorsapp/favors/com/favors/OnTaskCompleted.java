package favorsapp.favors.com.favors;

public interface OnTaskCompleted {
    void SetAutoComplete(String[] stringArray);
    void UpdateUI(String status, String event);
}
