package favorsapp.favors.com.favors;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import favorsapp.favors.com.favors.sync.GetChatCountsSync;
import favorsapp.favors.com.favors.sync.GetChatInfoSync;
import favorsapp.favors.com.favors.sync.GetChatUsersSync;

public class HomeActivity extends ActionBarActivity
        implements RegisterFragment.OnFragmentInteractionListener,
        LoginFragment.OnFragmentInteractionListener,
        FeedsFragment.OnFragmentInteractionListener,
        EditProfileFragment.OnFragmentInteractionListener,
        NearbyFavorsFragment.OnFragmentInteractionListener,
        BrowseFavorsFragment.OnFragmentInteractionListener,
        MyFavorsFragment.OnFragmentInteractionListener,
        AssignedFavorsFragment.OnFragmentInteractionListener,
        PopupMenu.OnMenuItemClickListener,
        ChatFragment.OnFragmentInteractionListener,
        NewFavorFragment.OnFragmentInteractionListener,
        ViewFavorFragment.OnFragmentInteractionListener,
        OnTaskCompleted,
        BeforeTaskExecution {

    private CharSequence mTitle;
    private boolean selectRegister;
    private String registerUsername;
    private String registerPassword;
    private String registerEmail;
    private FragmentManager homeFragmentManager;
    private FeedsFragment feedsFragment;
    private String sessionKey;
    private boolean runCountsThread;
    private SharedPreferences loginPrefs;
    private LogOutSync logOutSync;
    private HomeGetChatInfoSync getChatInfoSync;
    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private ChatUserMenuObject chatMenuObject;
    private OnTaskCompleted onTaskCompletedListener;
    private BeforeTaskExecution beforeTaskExecutionListener;
    private GetChatCountsSync chatCountsSync;
    public Handler countHandler;
    //Fragments
    public RegisterFragment regFragment;
    public LoginFragment loginFragment;
    public EditProfileFragment profileFragment;
    public int chatCount;
    public Thread countsThread;
    public String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        viewPager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new TabsStatePagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(pagerAdapter);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        slidingTabLayout.setViewPager(viewPager);
        slidingTabLayout.setDistributeEvenly(true);
        loginPrefs = getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        chatMenuObject = new ChatUserMenuObject();
       countHandler = new Handler();
        //buildChatWindow();

    }

    public void startThread() {
        runCountsThread = true;
        countsThread =
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(runCountsThread) {
                            try {
                                countHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        //String sessionKey = loginPrefs.getString("sessionKey", null);
                                        if(sessionKey != null) {
                                            chatCountsSync = new GetChatCountsSync(onTaskCompletedListener);
                                                chatCountsSync.execute(username);
                                        }
                                        if(Thread.interrupted()) {
                                            getChatInfoSync.cancel(true);
                                        }
                                    }
                                });
                                Thread.sleep(10000);
                            } catch(Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }}
                );
       countsThread.start();
    }


    public HomeActivity() {
        onTaskCompletedListener = this;
        beforeTaskExecutionListener = this;
        regFragment = new RegisterFragment();
        loginFragment = new LoginFragment();
        profileFragment = new EditProfileFragment();
        chatCount = 0;
    }

    public void onFragmentInteraction(Uri uri) {

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.home_section);
                break;
            case 2:
                mTitle = getString(R.string.new_favor_section);
                break;
            case 3:
                mTitle = getString(R.string.browse_favors_section);
                break;
            case 4:
                mTitle = getString(R.string.profile_section);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("Favors");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //pagerAdapter.notifyDataSetChanged();
        //viewPager.invalidate();
        //adjustTabs();
    }

    @Override
    public void onResume() {
        super.onResume();
        runCountsThread = true;
        pagerAdapter.notifyDataSetChanged();
        viewPager.invalidate();
        supportInvalidateOptionsMenu();
        sessionKey = loginPrefs.getString("sessionKey", null);
        username = loginPrefs.getString("username", null);
        try {
            startThread();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            runCountsThread = false;
            countsThread.interrupt();
            countsThread.join();
            countsThread = null;
        } catch(InterruptedException ie) {
            ie.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        setProfileName(menu);
        setChatCount(menu);
        return true;
    }

    public void setProfileName(Menu menu) {
        MenuItem profileButtonItem = menu.findItem(R.id.action_profile);
        String displayUserName = loginPrefs.getString("username", null);
        //TODO: User profile image instead of name text
        if (displayUserName == null) {
            displayUserName = "Guest";
        }
        profileButtonItem.setTitle(displayUserName);
    }

    public void setChatCount(Menu menu) {
        MenuItem chatCountButtonItem = menu.findItem(R.id.action_chat_count);
        String displayCount = String.valueOf(chatCount);
        chatCountButtonItem.setTitle(displayCount);
    }

    public void adjustTabs() {

    }

    public void showTabs() {
        slidingTabLayout.setVisibility(View.VISIBLE);
    }

    public void logOut() {
        Editor preferenceEdit = getSharedPreferences("loginPrefs", Context.MODE_PRIVATE).edit();
        preferenceEdit.clear();
        preferenceEdit.commit();
        pagerAdapter.notifyDataSetChanged();
        viewPager.invalidate();
        supportInvalidateOptionsMenu();
    }

    public void goToRegister() {
        regFragment = new RegisterFragment();
        FragmentTransaction registerTransaction = getSupportFragmentManager().beginTransaction();
        registerTransaction.replace(R.id.container, regFragment);
        registerTransaction.addToBackStack(null);
        registerTransaction.commit();

    }

    public void switchToActivity(String fragmentTag) {
        Intent userIntent = new Intent(this, UserActivity.class);
        userIntent.putExtra("fragment", fragmentTag);
        startActivity(userIntent);
    }

    public void switchToChat(String sender) {
        Intent userIntent = new Intent(this, UserActivity.class);
        userIntent.putExtra("fragment","chat");
        userIntent.putExtra("sender",sender);
        startActivity(userIntent);
    }

    public void switchToFavor(String favorid) {
        Intent userIntent = new Intent(this, UserActivity.class);
        userIntent.putExtra("fragment","favor");
        userIntent.putExtra("favorid",favorid);
        startActivity(userIntent);
    }

    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.guest_session_menu_register:
                switchToActivity("register");
                return true;
            case R.id.guest_session_menu_login:
                switchToActivity("login");
                return true;
            case R.id.guest_session_menu_logout:
                logOutSync = new LogOutSync();
                logOutSync.execute();
                return true;
            case R.id.guest_session_menu_profile:
                switchToActivity("profile");
                return true;
            default:
                String userString = chatMenuObject.getUserString(item);
                if(userString != null) {
                    switchToChat(userString);
                    return true;
                }
                else
                    return false;
        }
    }


    public void setActionBarImage() {

    }

    public PopupMenu buildChatMenu() {
        PopupMenu popUpMenu = null;
        String jsonString = "";
        try {
            HomeActivity.HomeGetChatUsersSync homeChatUsersSync = new HomeActivity.HomeGetChatUsersSync(loginPrefs);
            homeChatUsersSync.execute().get();
            jsonString = homeChatUsersSync.status;
            popUpMenu = chatMenuObject.buildChatMenu(this,jsonString,findViewById(R.id.action_chat_count),loginPrefs);
            popUpMenu.setOnMenuItemClickListener(this);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return popUpMenu;
    }


    public PopupMenu buildSessionMenu() {
        loginPrefs = getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        String displayUserName = loginPrefs.getString("username", null);
        PopupMenu guestSessionPopup = new PopupMenu(this, this.findViewById(R.id.action_profile));
        guestSessionPopup.setOnMenuItemClickListener(this);
        guestSessionPopup.getMenuInflater().inflate(R.menu.guest_session_menu, guestSessionPopup.getMenu());
        if (displayUserName == null) {
            guestSessionPopup.getMenu().findItem(R.id.guest_session_menu_login).setVisible(true);
            guestSessionPopup.getMenu().findItem(R.id.guest_session_menu_logout).setVisible(false);
            guestSessionPopup.getMenu().findItem(R.id.guest_session_menu_register).setVisible(true);
            guestSessionPopup.getMenu().findItem(R.id.guest_session_menu_profile).setVisible(false);
        } else {
            guestSessionPopup.getMenu().findItem(R.id.guest_session_menu_login).setVisible(false);
            guestSessionPopup.getMenu().findItem(R.id.guest_session_menu_logout).setVisible(true);
            guestSessionPopup.getMenu().findItem(R.id.guest_session_menu_register).setVisible(false);
            guestSessionPopup.getMenu().findItem(R.id.guest_session_menu_profile).setVisible(true);
        }
        return guestSessionPopup;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        selectRegister = true;
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        //TODO: Handle register users
        if (id == R.id.action_profile) {
            buildSessionMenu().show();
            return true;
        }
        if(id == R.id.action_chat) {
            switchToActivity("chatlist");
        }
        if(id == R.id.action_chat_count) {
            buildChatMenu().show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToFeedsFragment() {

    }

    public void drawNumberNotification() {
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public class LogOutSync extends AsyncTask<String, String, String> {
        protected String status;

        public LogOutSync() {
            status = "";
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            status = "fail";
            try {
                URL userInfoURL = new URL("http://45.55.133.160/favors/delete_session.html");
                String sessionKey = loginPrefs.getString("sessionKey", null);
                HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Cookie", "sessionid=" + sessionKey);
                int responseCode = connection.getResponseCode();
                BufferedReader inReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = inReader.readLine()) != null) {
                    status = line;
                    return line;
                }
                return "fail";
            } catch (MalformedURLException eURL) {
                return "fail";
            } catch (IOException e) {
                e.printStackTrace();
                return "fail";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            logOut();
        }
    }

    public static class TabsStatePagerAdapter extends FragmentStatePagerAdapter {

        final int PAGE_COUNT = 3;
        private Context context;
        private Fragment pFeedsFragment;
        private Fragment rootFragment;
        private Fragment pEditProfileFragment;
        private FragmentManager fm;


        public TabsStatePagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.fm = fm;
            this.context = context;
        }


        private final class PageListener implements RootFragmentListener {
            public void onSwitchToNextFragment(Fragment fragment) {
                notifyDataSetChanged();
            }
        }

        private PageListener pageListener = new PageListener();

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Nearby Favors";
                case 1:
                    return "My Favors";
                case 2:
                    return "Assigned Favors";
            }
            return "Null";
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0:
                    return new NearbyFavorsFragment();

                case 1:
                    return new MyFavorsFragment();

                case 2:
                    return new AssignedFavorsFragment();
            }
            return null;
        }

    }

    public interface RootFragmentListener {
        void onSwitchToNextFragment(Fragment fragment);
    }

    public class NotifCountIcon extends View {
        private Drawable drawableImage;

        public NotifCountIcon(Context context, AttributeSet attrs) {
            super(context, attrs);
            drawableImage = context.getResources().getDrawable(R.drawable.ic_drawer);
        }

        protected void onDraw(Canvas canvas) {
            Rect imageBounds = canvas.getClipBounds();
            drawableImage.setBounds(canvas.getClipBounds());
            Paint p = new Paint();
            p.setColor(Color.RED);
            canvas.drawText("text",10,10,p);
            drawableImage.draw(canvas);
        }
    }

    void refreshActionBar(String status) {
        try {
            chatCount = Integer.parseInt(status);
            this.invalidateOptionsMenu();
        } catch(Exception e) {

        }
    }

    public class HomeGetChatInfoSync extends GetChatInfoSync {

        public HomeGetChatInfoSync(SharedPreferences loginPrefs) {
            super(beforeTaskExecutionListener,onTaskCompletedListener);
        }

        @Override
        protected void onPostExecute(String result) {
            refreshActionBar(status);
            //disconnect();
        }

    }

    public class HomeGetChatUsersSync extends GetChatUsersSync {

        public HomeGetChatUsersSync(SharedPreferences loginPrefs) { super(loginPrefs); }

        @Override
        protected void onPostExecute(String result) { }
    }

    public void PrepareExecution(String event) {

    }

    public void UpdateUI(String status, String event) {
        if(event.equals("Get Chat Count"))
            refreshActionBar(status);
    }

    public void SetAutoComplete(String[] stringArray) {

    }

}
