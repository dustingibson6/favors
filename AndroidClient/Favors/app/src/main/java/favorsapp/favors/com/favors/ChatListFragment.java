package favorsapp.favors.com.favors;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import favorsapp.favors.com.favors.sync.GetChatInfoSync;

public class ChatListFragment extends Fragment implements
        OnTaskCompleted, BeforeTaskExecution
{

    private OnFragmentInteractionListener mListener;
    private GetChatInfoSync chatInfoSync;
    private SharedPreferences loginPrefs;
    private ObjectAnimator progressAnimator;
    private ProgressBar progressBar;
    private OnTaskCompleted onTaskCompletedListener;
    private BeforeTaskExecution beforeTaskExecution;
    public List<ListInfo> listInfo;
    public String sessionKey;
    public ListView chatListView;
    public ChatListAdapter chatListAdapter;
    public ChatFragment chatFragment;
    public LinearLayout layoutListProgress;
    public Handler countHandler;
    public Thread chatListThread;
    public boolean runThread;

    public static ChatListFragment newInstance(String param1, String param2) {
        ChatListFragment fragment = new ChatListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ChatListFragment() {
        beforeTaskExecution = this;
        onTaskCompletedListener = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    public void goToChat(String sender) {
        ((UserActivity)this.getActivity()).goToChat(sender);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        listInfo = new ArrayList<ListInfo>();
        View fragmentView =  inflater.inflate(R.layout.fragment_chat_list, container, false);
        loginPrefs = getActivity().getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        sessionKey = loginPrefs.getString("sessionKey", null);
        layoutListProgress = (LinearLayout) fragmentView.findViewById(R.id.layoutListProgress);
        progressBar = (ProgressBar) fragmentView.findViewById(R.id.chatListProgress);
        progressAnimator = ObjectAnimator.ofInt(progressBar, "progress",0,100);
        progressAnimator.setDuration(1000);
        //this.getActivity().requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        //this.getActivity().setProgressBarVisibility(true);
        chatListView = (ListView) fragmentView.findViewById(R.id.chatListView);
        chatInfoSync = new GetChatInfoSync(beforeTaskExecution,onTaskCompletedListener);
        //chatInfoSync.execute();

        countHandler = new Handler();
        runThread = true;
        //buildChatWindow();
        //Thread chatListTHread =
        chatListThread =
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(runThread) {
                    try {
                        countHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if(Thread.interrupted()) {
                                    chatInfoSync.cancel(true);
                                    return;
                                }
                                String sessionKey = loginPrefs.getString("sessionKey", null);
                                if(sessionKey != null && Thread.interrupted() == false) {
                                    chatInfoSync = new GetChatInfoSync(beforeTaskExecution,onTaskCompletedListener);
                                    chatInfoSync.execute(sessionKey);
                                }
                            }
                        });
                        Thread.sleep(10000);
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }}
        );
        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        chatListThread.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            runThread = false;
            chatListThread.interrupt();
            chatListThread.join();
            //chatListThread.interrupt();
            if (chatInfoSync != null)
                chatInfoSync.cancel(true);
        } catch(InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    public List<String> createRowList(List<ListInfo> chatList) {
        List<String> rowList = new ArrayList<String>();
        for(int i = 0; i < chatList.size(); i++) {
            rowList.add(chatList.get(i).sender);
        }
        return rowList;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public class ChatListAdapter extends ArrayAdapter<String> {
        HashMap<Integer, ListInfo> listMap = new HashMap<Integer,ListInfo>();
        private Activity context;

        public ChatListAdapter(Activity context, int resourceId, List<ListInfo> objects, List<String> rowList) {
            super(context, resourceId, rowList);
            this.context = context;
            for(int i = 0; i < objects.size(); i++)
                listMap.put(i,objects.get(i));
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(convertView == null) {
                LayoutInflater inflator = context.getLayoutInflater();
                view = inflator.inflate(R.layout.chat_list_layout,null);
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.userLabel = (TextView) view.findViewById(R.id.userLabel);
                viewHolder.messageLabel = (TextView) view.findViewById(R.id.messageLabel);
                viewHolder.dateLabel = (TextView) view.findViewById(R.id.dateLabel);
                viewHolder.profileImage = (ImageView) view.findViewById(R.id.chatListProfileImage);
                viewHolder.userLabel.setTypeface(null, Typeface.BOLD);
                viewHolder.userLabel.setTextSize(16);
                view.setTag(viewHolder);
            }
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            ListInfo currentInfo = listMap.get(position);
            String userString = currentInfo.sender;
            String messageString = currentInfo.message;
            String dateString = currentInfo.date;
            String imageString = currentInfo.imagePath;
            viewHolder.userLabel.setText(userString);
            viewHolder.messageLabel.setText(messageString);
            viewHolder.dateLabel.setText(dateString);
            viewHolder.profileImage.setImageDrawable(FavorsGlobal.getInstance().loadImageFromWeb(imageString));
            return view;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }

    public void PrepareExecution(String event) {
        layoutListProgress.setVisibility(View.VISIBLE);
        progressAnimator.start();
    }

    public void UpdateUI(String status, String event) {
        refreshList(status);
        layoutListProgress.setVisibility(View.GONE);
    }

    public void SetAutoComplete(String[] stringArray) {

    }


    public void refreshList(String jsonString) {
        listInfo = new ArrayList<ListInfo>();
        try {
            JSONArray chatListJSON = new JSONArray(jsonString);
            for(int i = 0; i < chatListJSON.length(); i++) {
                JSONObject currentObject = chatListJSON.getJSONObject(i);
                String message = currentObject.getString("message");
                //String sender = currentObject.getString("sender");
                //String sender = currentObject.getString("sender");
                String userName = currentObject.getString("sender");
                String firstName = currentObject.getString("firstname");
                String lastName = currentObject.getString("lastname");
                String sender = FavorsGlobal.getInstance().getDisplayName(userName, firstName, lastName);
                String date = currentObject.getString("date");
                String imagePath = currentObject.getString("image");
                ListInfo newInfo = new ListInfo(userName,sender,message,date,imagePath);
                listInfo.add(i,newInfo);
            }
        } catch(JSONException jsonE) {
            jsonE.printStackTrace();
        }
        sortListByDate();
        List<String> rowList = createRowList(listInfo);
        chatListAdapter = new ChatListAdapter(this.getActivity(),R.layout.chat_list_layout,listInfo,rowList);
        chatListView.setAdapter(chatListAdapter);

        chatListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                //final String item = (String) parent.getItemAtPosition(position);
                final String item = (String) listInfo.get(position).userName;
                goToChat(item);
            }
        });

    }

    public void sortListByDate() {
        if(listInfo.size() <= 1)
            return;
       DateFormat utcDateFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
       utcDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
       DateFormat localDateFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
       localDateFormat.setTimeZone(DateTime.now().getZone().toTimeZone());
        try {
            boolean done = false;
            while(!done) {
                done = true;
                for(int i = 1; i < listInfo.size(); i++) {
                    ListInfo tmpListInfo;
                    Date dateA = utcDateFormat.parse(listInfo.get(i).date);
                    Date dateB = utcDateFormat.parse(listInfo.get(i-1).date);
                    if(dateA.after(dateB)) {
                        tmpListInfo = listInfo.get(i);
                        listInfo.set(i,listInfo.get(i-1));
                        listInfo.set(i-1,tmpListInfo);
                        done = false;
                    }
                }
            }
            for(int i = 0; i < listInfo.size(); i++) {
                DateTime curServerDate = new DateTime(utcDateFormat.parse(listInfo.get(i).date).getTime());
                //DateTime newCurServerDate = curServerDate.toDateTime(DateTime.now().getZone());
                listInfo.get(i).date = localDateFormat.format(curServerDate.toDateTime(DateTime.now().getZone()).toDate());
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


     static class ViewHolder {
        TextView userLabel;
        TextView messageLabel;
        TextView dateLabel;
        ImageView profileImage;
    }

    public class ListInfo {
        public String sender;
        public String message;
        public String date;
        public String imagePath;
        public String userName;

        ListInfo(String userName, String sender, String message,String date, String imagePath) {
            this.message = message;
            this.userName = userName;
            this.sender = sender;
            this.date = date;
            this.imagePath = imagePath;
        }
    }

}
