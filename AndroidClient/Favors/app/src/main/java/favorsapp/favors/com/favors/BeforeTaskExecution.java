package favorsapp.favors.com.favors;

public interface BeforeTaskExecution {
    void PrepareExecution(String event);
}
