package favorsapp.favors.com.favors;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import favorsapp.favors.com.favors.sync.GetAutoLocationSync;
import favorsapp.favors.com.favors.sync.RegisterSync;

public class RegisterFragment extends Fragment implements OnTaskCompleted {

    private String descriptionText;
    public RegisterSync RegSync;
    private OnFragmentInteractionListener mListener;
    private OnTaskCompleted onTaskCompletedListener;
    private String autoZip;
    private GetAutoLocationSync autoLocationSync;
    protected ArrayAdapter<String> autoAdapter;
    protected AutoCompleteTextView autoLocationText;
    public static HomeActivity.RootFragmentListener rootFragmentListener;
    public String sessionKey;
    public SharedPreferences loginPrefs;

    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public RegisterFragment() {
        onTaskCompletedListener = this;
    }

    public static RegisterFragment newInstance(HomeActivity.RootFragmentListener fl) {
        rootFragmentListener = fl;
        RegisterFragment fragment = new RegisterFragment();
        return fragment;
    }


    private int registerUser() {
        EditText usernameWidget = (EditText) getView().findViewById(R.id.usernameText);
        EditText verifyPasswordWidget = (EditText) getView().findViewById(R.id.verifyPasswordText);
        EditText passwordWidget = (EditText) getView().findViewById(R.id.passwordText);
        EditText emailWidget = (EditText) getView().findViewById(R.id.emailText);
        EditText zipWidget = (EditText) getView().findViewById(R.id.zipcodeText);
        String verifyPasswordText = verifyPasswordWidget.getText().toString();
        String passwordText = passwordWidget.getText().toString();
        String usernameText = usernameWidget.getText().toString();
        String emailText = emailWidget.getText().toString();
        String zipText = zipWidget.getText().toString();
        if ( !(zipWidget.length() < 6 || zipWidget.length() > 6) )
            return PostStatus("Invalid location");
        else if (!(passwordText.length() <= 30))
            return PostStatus("Password must be no more than 30 characters");
        else if(!(usernameText.length() <= 30))
            return PostStatus("Username must be no more than 30 characters");
        else if (!(usernameText.length() >= 4))
            return PostStatus("Username must be more than 4 characters");
        else if(!(passwordText.length() >= 6))
            return PostStatus("Passwords must have 6 or more characters");
        else if(!passwordText.equals(verifyPasswordText))
            return PostStatus("Password do not match");
        Button registerButton = (Button) getView().findViewById(R.id.registerButton);
        registerButton.setEnabled(false);
        registerButton.setText("Sending...");
        RegSync = new RegisterSync(onTaskCompletedListener);
        RegSync.execute(usernameText, passwordText, emailText, zipText);
        return 1;
    }

    public void goBackHome() {
        FragmentActivity fragActivity = this.getActivity();
        FeedsFragment feedsFragment = (FeedsFragment) fragActivity.getSupportFragmentManager().findFragmentById(R.id.HomeFragment);
        if( feedsFragment == null ) {
           feedsFragment = new FeedsFragment();
            FragmentTransaction feedsTransaction = fragActivity.getSupportFragmentManager().beginTransaction();
            feedsTransaction.replace(R.id.container,feedsFragment);
            //feedsTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            feedsTransaction.commit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_register, container, false);
        Button registerButton = (Button) fragmentView.findViewById(R.id.registerButton);
        Button cancelButton = (Button) fragmentView.findViewById(R.id.cancelButton);
        autoLocationText = (AutoCompleteTextView) fragmentView.findViewById(R.id.zipcodeText);
        loginPrefs = getActivity().getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        sessionKey = loginPrefs.getString("sessionKey", null);
        registerButton.setOnClickListener (
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        registerUser();
                    }
                });
        cancelButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        getActivity().onBackPressed();
                    }
                });
        autoLocationText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                autoZip = autoLocationText.getText().toString();
                autoLocationSync = new GetAutoLocationSync(onTaskCompletedListener);
                autoLocationSync.execute(autoZip);
            }

        });
        return fragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(RegSync != null)
            RegSync.cancel(true);
    }



    public int PostStatus(String status) {
        FragmentActivity currentActivity = this.getActivity();
        TextView descriptionTextView = (TextView)getView().findViewById(R.id.descriptionTextView);
        descriptionText = descriptionTextView.getText().toString();
        switch(status) {
            case "Email and/or Username Exists":
                descriptionText = "Email and/or Username Exists";
                break;

            case "Password Do Not Match":
                descriptionText = "Passwords do not match. Re-enter password...";
                break;

            case "Success":
                descriptionText = "Registered";
                //TODO: Dialog box informing user is registered
                ((UserActivity)this.getActivity()).switchToLogin();
                return 1;
            //descriptionText = "Success";
            //break;

            case "password less than 6 chars":
                descriptionText = "Your password has less than 6 characters. Enter a password 6 characters or more:";
                break;

            case "username less than 4 chars":
                descriptionText = "Your username has less than 4 characters. Enter a username with 4 characters or more:";
                break;

            case "username over 30 chars":
                descriptionText = "Your username has more than 30 characters. Enter a username with less than 30 characters:";
                break;

            case "password over 30 chars":
                descriptionText = "Your password has more than 30 characters. Enter a password with less than 30 characters:";
                break;

            default:
                descriptionText = "Something is wrong with server";
                break;
        }
        descriptionTextView.setTextColor(Color.RED);
        descriptionTextView.setText(descriptionText);
        return 0;
    }

    public void SetAutoComplete(String[] stringArray)
    {
        if(stringArray.length > 0) {
            autoAdapter = new ArrayAdapter<String>(getActivity(),
                    R.layout.list_autolocation,
                    R.id.autolocationText,
                    stringArray);
            autoLocationText.setAdapter(autoAdapter);
            if (!autoLocationText.isPopupShowing())
                autoLocationText.showDropDown();
        }
    }

    public void UpdateUI(String status, String event) {
        PostStatus(status);
        Button registerButton = (Button)getView().findViewById(R.id.registerButton);
        registerButton.setEnabled(true);
        registerButton.setText("Register");
    }

}
