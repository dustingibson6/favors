package favorsapp.favors.com.favors;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import favorsapp.favors.com.favors.sync.GetMyFavorsSync;


public class MyFavorsFragment extends Fragment implements OnTaskCompleted, BeforeTaskExecution {

    public Bitmap favorImage;
    private OnFragmentInteractionListener mListener;
    private GetMyFavorsSync getMyFavorsSync;
    private OnTaskCompleted onTaskCompletedListener;
    private BeforeTaskExecution beforeTaskExecutionListener;
    private ExpandableListView myFavorsListView;
    private SharedPreferences loginPrefs;
    protected ExpandableListAdapter myFavorsListAdapter;
    protected String sessionKey;

    public MyFavorsFragment() {
        onTaskCompletedListener = this;
        beforeTaskExecutionListener = this;
    }

    public static MyFavorsFragment newInstance() {
        MyFavorsFragment fragment = new MyFavorsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_favors, container, false);
        ViewGroup parent = (ViewGroup) view.getParent();
        if(parent != null)
            parent.removeView(view);
        Button newFavorButton = (Button) view.findViewById(R.id.newFavorButton);
        myFavorsListView = (ExpandableListView) view.findViewById(R.id.myFavorListView);
        newFavorButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        ((HomeActivity) getActivity()).switchToActivity("new favor");
                    }
                });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onResume() {
        loginPrefs = getActivity().getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        sessionKey = loginPrefs.getString("sessionKey", null);
        getMyFavorsSync = new GetMyFavorsSync(beforeTaskExecutionListener, onTaskCompletedListener);
        getMyFavorsSync.execute(sessionKey);
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    public void SetAutoComplete(String[] stringArray)
    {

    }

    public void UpdateUI(String status, String event)
    {
        updateList(status);
    }

    public void PrepareExecution(String event) {

    }

    static class ViewHolderParent {
        TextView favorsTitleText;
        TextView deadlineText;
        ImageView favorsImage;
    }

    static class ViewHolderChild {
        TextView candidateText;
        ImageView profileImage;
    }

    public static class ListInfo {
        String userID;
        String name;
        String imagePath;
        Drawable profileImage;

        public ListInfo(String userID, String username, String firstname, String lastname, String imagePath ) {
            this.userID = userID;
            this.name = FavorsGlobal.getInstance().getDisplayName(username,firstname,lastname);
            this.imagePath = imagePath;
            if(!imagePath.equals("None")) {
                profileImage = FavorsGlobal.getInstance().loadImageFromWeb(imagePath);
            }
        }
    }

    public static class RowInfo {
        String favorName;
        String favorID;
        String favorPath;
        String deadline;
        Drawable favorImage;

        public RowInfo(String favorID, String favorName, String deadline, String favorPath) {
            this.favorID = favorID;
            this.favorName = favorName;
            this.favorPath = favorPath;
            DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a");
            DateTime deadlineDateTime = formatter.parseDateTime(deadline);
            DateTime currentDateTime = DateTime.now();
            this.deadline = FavorsGlobal.getInstance().GetTimeLeftString(currentDateTime,deadlineDateTime);
            if(!favorPath.equals("None")) {
                favorImage = FavorsGlobal.getInstance().loadImageFromWeb("thumb/" + favorPath);
            }
        }

    }

    public void updateList(String jsonString) {
        if(jsonString.equals("fail"))
            return;
        try {
            final ArrayList<ArrayList<ListInfo>> listInfo = new ArrayList<ArrayList<ListInfo>>();
            final ArrayList<RowInfo> rowInfo = new ArrayList<RowInfo>();
            ArrayList<String> rows = new ArrayList<String>();
            JSONArray myFavors = new JSONArray(jsonString);
            for(int i=0; i < myFavors.length(); i++) {
                ArrayList<ListInfo> tempList = new ArrayList<ListInfo>();
                JSONObject myFavorsObject = myFavors.getJSONObject(i);
                String favorID = myFavorsObject.getString("favorid");
                String favorName = myFavorsObject.getString("title");
                String favorImagePath = myFavorsObject.getString("favorimage");
                String deadline = myFavorsObject.getString("deadline");
                rowInfo.add(new RowInfo(favorID,favorName,deadline,favorImagePath));
                rows.add(favorName);
                JSONArray myCandidates = myFavorsObject.getJSONArray("candidates");
                for(int j = 0; j < myCandidates.length(); j++) {
                    JSONObject myCandidateObject = myCandidates.getJSONObject(j);
                    String userID = myCandidateObject.getString("userid");
                    String firstname = myCandidateObject.getString("firstname");
                    String lastname = myCandidateObject.getString("lastname");
                    String username = myCandidateObject.getString("username");
                    String candidateImagePath = myCandidateObject.getString("candidateimage");
                    tempList.add(new ListInfo(userID,username,firstname,lastname,candidateImagePath));
                }
                listInfo.add(tempList);
            }
            LayoutInflater inflater = (LayoutInflater)this.getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
            myFavorsListAdapter = new MyFavorsListAdapter(this.getActivity(),inflater,rowInfo,listInfo);
            myFavorsListView.setAdapter(myFavorsListAdapter);
        } catch(JSONException jsonE) {
            jsonE.printStackTrace();
        }
    }

    public class MyFavorsListAdapter extends BaseExpandableListAdapter {
        ArrayList<ArrayList<ListInfo>> listInfo = new ArrayList<ArrayList<ListInfo>>();
        ArrayList<RowInfo> rowInfo = new ArrayList<RowInfo>();
        public LayoutInflater childFlater;
        private Activity context;

        public MyFavorsListAdapter(Activity context, LayoutInflater inflater, ArrayList<RowInfo> rowObjects,  ArrayList<ArrayList<ListInfo>> objects) {
            this.rowInfo = rowObjects;
            this.listInfo = objects;
            childFlater = inflater;
            this.context = context;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return null;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition, boolean lastChild, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = childFlater.inflate(R.layout.my_favors_sublist, null);
            }
            ViewHolderChild viewHolder = new ViewHolderChild();
            viewHolder.candidateText = (TextView)convertView.findViewById(R.id.myFavorCandidateName);
            viewHolder.profileImage = (ImageView) convertView.findViewById(R.id.myFavorProfileIcon);
            ListInfo currentInfo = listInfo.get(groupPosition).get(childPosition);
            viewHolder.candidateText.setText(currentInfo.name);
            viewHolder.profileImage.setImageDrawable(currentInfo.profileImage);
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return  listInfo.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return null;
        }

        @Override
        public int getGroupCount() {
            return rowInfo.size();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
            super.onGroupCollapsed(groupPosition);
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
            super.onGroupExpanded(groupPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = childFlater.inflate(R.layout.my_favors_list,null);
            }
            //Set up parent UI
            ViewHolderParent viewHolderParent = new ViewHolderParent();
            RowInfo currentRow = rowInfo.get(groupPosition);
            viewHolderParent.deadlineText = (TextView)convertView.findViewById(R.id.myFavorDeadline);
            viewHolderParent.favorsTitleText = (TextView)convertView.findViewById(R.id.myFavorTitle);
            viewHolderParent.favorsImage = (ImageView)convertView.findViewById(R.id.myFavorIcon);
            viewHolderParent.deadlineText.setText(currentRow.deadline);
            viewHolderParent.favorsTitleText.setText(currentRow.favorName);
            viewHolderParent.favorsImage.setImageDrawable(currentRow.favorImage);
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }

    }

}
