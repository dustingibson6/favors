package favorsapp.favors.com.favors;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import favorsapp.favors.com.favors.sync.GetFavorsListSync;

public class NearbyFavorsFragment extends Fragment implements OnTaskCompleted {

    private OnFragmentInteractionListener mListener;
    protected String sessionKey;
    protected String selectedFavorID;
    protected String zip;
    private SharedPreferences loginPrefs;
    private GetFavorsListSync getFavorsListSync;
    private OnTaskCompleted onTaskCompletedListener;
    private ListView nearbyListView;

    protected NearbyListAdapter nearbyListAdapter;

    public static NearbyFavorsFragment newInstance() {
        NearbyFavorsFragment fragment = new NearbyFavorsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public NearbyFavorsFragment() {
        onTaskCompletedListener = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_nearby_favors, container, false);
        nearbyListView = (ListView) fragmentView.findViewById(R.id.nearbyListView);
        return fragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onResume() {
        loginPrefs = getActivity().getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        sessionKey = loginPrefs.getString("sessionKey", null);
        getFavorsListSync = new GetFavorsListSync(onTaskCompletedListener);
        zip = loginPrefs.getString("zip",null);
        getFavorsListSync.execute(sessionKey,zip);
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    public class NearbyListAdapter extends ArrayAdapter<String> {
        ArrayList<ListInfo> listInfo = new ArrayList<ListInfo>();
        private Activity context;

        public NearbyListAdapter(Activity context, int resourceId, ArrayList<ListInfo> objects,ArrayList<String> rows) {
            super(context, resourceId,rows);
            this.context = context;
            this.listInfo = objects;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(convertView == null) {
                LayoutInflater inflator = context.getLayoutInflater();
                view = inflator.inflate(R.layout.nearby_list_layout,null);
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.name = (TextView) view.findViewById(R.id.nearbyFavorsName);
                viewHolder.name.setTypeface(null, Typeface.BOLD);
                viewHolder.location = (TextView) view.findViewById(R.id.nearbyFavorLocation);
                viewHolder.icon = (ImageView) view.findViewById(R.id.nearbyFavorsIcon);
                viewHolder.timeLeft = (TextView) view.findViewById(R.id.nearbyFavorDeadline);
                viewHolder.location.setTextSize(14);
                viewHolder.timeLeft.setTextSize(14);
                view.setTag(viewHolder);
            }
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            ListInfo currentListInfo = listInfo.get(position);
            String locationLabel = currentListInfo.city + ", " +
                    currentListInfo.state + " (" + currentListInfo.zip + ") " +
                    currentListInfo.distance + " miles away";
            String nameLabel = currentListInfo.title;
            String path = "thumb/"+currentListInfo.imagePath;
            viewHolder.name.setText(nameLabel);
            viewHolder.location.setText(locationLabel);
            viewHolder.timeLeft.setText(currentListInfo.timeLeft);
            if( currentListInfo.status.equals("Interested")) {
                view.setBackgroundColor(Color.rgb(144, 238, 144));
            }
            else if(currentListInfo.status.equals("Not Interested")) {
                view.setBackgroundColor(Color.rgb(250, 128, 114));
            }
            if(!currentListInfo.imagePath.equals("None")) {
                viewHolder.icon.setImageDrawable(currentListInfo.image);
            }
            return view;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }

    static class ViewHolder {
        TextView name;
        TextView location;
        TextView timeLeft;
        ImageView icon;
    }

    static class ListInfo {
        String title;
        String distance;
        String city;
        String zip;
        String state;
        String imagePath;
        int intDistance;
        String favorID;
        Drawable image;
        String timeLeft;
        String status;

        public ListInfo(String title, String status, String zip, String city, String state, String distance, String imagePath, String favorID, String deadline) {
            this.timeLeft = "";
            this.title = title;
            this.distance = distance;
            this.zip = zip;
            this.city = city;
            this.state = state;
            this.intDistance = Integer.parseInt(distance);
            this.imagePath = imagePath;
            this.favorID = favorID;
            this.status = status;

            DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a");
            DateTime deadlineDateTime = formatter.parseDateTime(deadline);
            DateTime currentDateTime = DateTime.now();

            this.timeLeft = FavorsGlobal.getInstance().GetTimeLeftString(currentDateTime,deadlineDateTime);

            try {
                if(!imagePath.equals("None")) {
                    this.image = FavorsGlobal.getInstance().loadImageFromWeb("thumb/" + imagePath);
                }
            } catch(NullPointerException ex) {

            }
        }
    }

    public void updateList(String jsonString) {
        if(jsonString.equals("fail"))
            return;
        try {
            final ArrayList<ListInfo> listInfo = new ArrayList<ListInfo>();
            ArrayList<String> rows = new ArrayList<String>();
            JSONArray nearbyFavors = new JSONArray(jsonString);
            for (int i = 0; i < nearbyFavors.length(); i++) {
                JSONObject currentObject = nearbyFavors.getJSONObject(i);
                String title = currentObject.getString("title");
                String city = currentObject.getString("city");
                String state = currentObject.getString("state");
                String zip = currentObject.getString("zip");
                String distance = currentObject.getString("distance");
                String imagePath = currentObject.getString("thumb");
                String favorID = currentObject.getString("favorid");
                String deadline = currentObject.getString("deadline");
                String status = currentObject.getString("status");
                ListInfo newListInfo = new ListInfo(title,status,zip,city,state,distance,imagePath,favorID,deadline);
                rows.add(title);
                listInfo.add(newListInfo);
            }
            for(int i=0; i < listInfo.size(); i++)
                for(int j=0; j < i; j++) {
                    if(listInfo.get(i).intDistance < listInfo.get(j).intDistance) {
                        ListInfo temp = listInfo.get(j);
                        listInfo.set(j, listInfo.get(i));
                        listInfo.set(i, temp);
                    }
                }
            nearbyListAdapter = new NearbyListAdapter(this.getActivity(),R.layout.nearby_list_layout,listInfo,rows);
            nearbyListView.setAdapter(nearbyListAdapter);
            nearbyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                    selectedFavorID = listInfo.get(position).favorID.toString();
                    ((HomeActivity)getActivity()).switchToFavor(selectedFavorID);
                }
            });
        } catch(JSONException jsonE) {
            jsonE.printStackTrace();
        }
    }

    public void SetAutoComplete(String[] stringArray)
    {

    }

    public void UpdateUI(String status, String event)
    {
        updateList(status);
    }

}
