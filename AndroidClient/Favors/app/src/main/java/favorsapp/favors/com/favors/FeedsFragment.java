package favorsapp.favors.com.favors;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.content.Intent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FeedsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private UserInfoSync userInfoSync;
    private SharedPreferences loginPrefs;
    private static HomeActivity.RootFragmentListener rootFragmentListener;
    public String userInfo;
    private ExpandableListView listView;
    private ExpandableListView myFavorsListView;
    private contentExpandableListAdapter contentListAdapter;
    private contentExpandableListAdapter myFavorsContentListAdapter;
    private List<String> contentDataHeader;
    private HashMap<String, List<String>> contentDataChild;

    public static FeedsFragment newInstance(String param1, String param2) {
        FeedsFragment fragment = new FeedsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static FeedsFragment newInstance(HomeActivity.RootFragmentListener fl) {
        rootFragmentListener = fl;
        FeedsFragment fragment = new FeedsFragment();
        return fragment;
    }

    public FeedsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        loginPrefs = getActivity().getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    private void prepareList() {
        contentDataHeader = new ArrayList<String>();
        contentDataChild = new HashMap<String, List<String>>();
        contentDataHeader.add("Test Header");
        contentDataHeader.add("Test Header 2");
        List<String> listContent = new ArrayList<String>();
        listContent.add("content 1");
        listContent.add("content 2");
        listContent.add("content 1");
        listContent.add("content 2");
        listContent.add("content 1");
        listContent.add("content 2");
        listContent.add("content 1");
        listContent.add("content 2");
        listContent.add("content 1");
        listContent.add("content 2");
        listContent.add("content 1");
        listContent.add("content 2");
        listContent.add("content 1");
        listContent.add("content 2");
        listContent.add("content 1");
        listContent.add("content 2");
        List<String> listContent2 = new ArrayList<String>();
        listContent2.add("content 3");
        listContent2.add("content 4");
        contentDataChild.put(contentDataHeader.get(0), listContent);
        contentDataChild.put(contentDataHeader.get(1), listContent2);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void switchToRegister() {
        Intent userIntent = new Intent(this.getActivity(), UserActivity.class);
        userIntent.putExtra("fragment","register");
        startActivity(userIntent);
    }

    private void switchToLogin() {
        HomeActivity currentActivity = (HomeActivity) this.getActivity();
        currentActivity.loginFragment = new LoginFragment();
        FragmentTransaction loginTransaction = currentActivity.getSupportFragmentManager().beginTransaction();
        loginTransaction.replace(R.id.container, currentActivity.loginFragment);
        loginTransaction.addToBackStack(null);
        loginTransaction.commit();
        currentActivity.adjustTabs();
    }

    private void switchToEditProfile() {
        HomeActivity currentActivity = (HomeActivity) this.getActivity();
        currentActivity.profileFragment = new EditProfileFragment();
        FragmentTransaction profileTransaction = currentActivity.getSupportFragmentManager().beginTransaction();
        profileTransaction.replace(R.id.container, currentActivity.profileFragment);
        profileTransaction.addToBackStack(null);
        profileTransaction.commit();
        currentActivity.adjustTabs();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        userInfoSync = new UserInfoSync();
        userInfoSync.execute();
        View fragmentView = inflater.inflate(R.layout.fragment_feeds, container, false);
        Button loginButton = (Button) fragmentView.findViewById(R.id.feedsLoginButton);
        Button registerButton = (Button) fragmentView.findViewById(R.id.feedsRegisterButton);
        Button editProfileButton = (Button) fragmentView.findViewById(R.id.feedsEditProfileButton);
        prepareList();
        //myFavorsListView =  (ExpandableListView) fragmentView.findViewById(R.id.myFavorsListView);
        listView = (ExpandableListView) fragmentView.findViewById(R.id.favorsListView);
        contentListAdapter = new contentExpandableListAdapter(inflater, contentDataHeader, contentDataChild, listView);
        listView.setAdapter(contentListAdapter);
        registerButton.setOnClickListener (
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        switchToRegister();
                    }
                });
        loginButton.setOnClickListener (
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        switchToLogin();
                    }
                }
        );
        editProfileButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        switchToEditProfile();
                    }
                }
        );
        return fragmentView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
        }
        return false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    public void getUserInfo(String results) {
        //TextView userTextView = (TextView)getView().findViewById(R.id.testTextView);
        //userTextView.setText(results);
    }

    private class contentExpandableListAdapter extends BaseExpandableListAdapter
    implements SectionIndexer {
        private final LayoutInflater layoutInflater;
        private List<String> dataHeader;
        private HashMap<String, List<String>> dataChild;
        private ExpandableListView listView;

        public contentExpandableListAdapter(LayoutInflater layoutInflater, List<String> dataHeaders,  HashMap<String, List<String>> dataChild, ExpandableListView listView ) {
            this.dataHeader = dataHeaders;
            this.dataChild = dataChild;
            this.layoutInflater = layoutInflater;
            this.listView = listView;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parentView) {
            String headerTitle = (String) getGroup(groupPosition);
            if(convertView == null) {
                //LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.favors_list_group, null);
            }
            TextView headerText = (TextView) convertView.findViewById(R.id.favorsListGroupText);
            headerText.setText(headerTitle);
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parentView) {
            final String childText = (String) getChild(groupPosition,childPosition);
            if(convertView == null) {
                //LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.favors_list_child, null);
            }
            TextView textChild = (TextView) convertView.findViewById(R.id.favorsListChildText);
            textChild.setText(childText);
            return convertView;
        }


        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return this.dataChild.get(this.dataHeader.get(groupPosition)).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition)  {
            return childPosition;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return dataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this.dataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this.dataChild.get(this.dataHeader.get(groupPosition)).size();
        }

        @Override
        public int getPositionForSection(int section) {
            return listView.getFlatListPosition((ExpandableListView.getPackedPositionForGroup(section)));
        }

        @Override
        public int getSectionForPosition(int position) {
            return ExpandableListView.getPackedPositionGroup(listView.getExpandableListPosition(position));
        }

        @Override
        public Object[] getSections() {
            return dataHeader.toArray();
        }

    }


    public class UserInfoSync extends AsyncTask<String,String,String> {
        protected String status;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            status = "fail";
            try {
                URL userInfoURL = new URL("http://45.55.133.160/favors/userinfo.html");
                String sessionKey = loginPrefs.getString("sessionKey",null);
                HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Cookie","sessionid=" + sessionKey);
                int responseCode = connection.getResponseCode();
                BufferedReader inReader = new BufferedReader( new InputStreamReader(connection.getInputStream()));
                String line;
                StringBuffer response = new StringBuffer();
                while((line = inReader.readLine()) != null) {
                    status = line;
                    return line;
                }
                return "fail";
            } catch (MalformedURLException eURL) {
                return "fail";
            } catch (IOException e) {
                e.printStackTrace();
                return "fail";
            }
        }


        @Override
        protected void onPostExecute(String result) {
            getUserInfo(status);
        }
    }
}
