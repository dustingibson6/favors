package favorsapp.favors.com.favors;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TimePicker;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class NewFavorFragment extends Fragment {

    protected String title;
    protected String duration;
    protected String reward;
    protected String description;
    protected String participants;
    protected String location;
    protected String deadline;
    protected int this_day;
    protected int this_month;
    protected int this_year;
    protected int this_hour;
    protected int this_minute;
    protected int this_am_pm;
    protected static int[] imagePreviewIDs;
    protected static ImageView[] imagePreviews;
    protected int favorId;
    protected UploadImageSync uploadImageSync;
    protected List<Bitmap> imageList;
    protected ImageView previewImage;
    protected EditText deadlineText;
    private OnFragmentInteractionListener mListener;
    private SharedPreferences loginPrefs;
    private PostFavorSync postFavorSync;
    private static int maxImages;


    public static NewFavorFragment newInstance(String param1, String param2) {
        NewFavorFragment fragment = new NewFavorFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public NewFavorFragment() {
        imageList = new ArrayList<Bitmap>();
        favorId = -1;
        maxImages = 5;
        imagePreviewIDs = new int[]{
                R.id.newFavorPreviewImage,
                R.id.newFavorPreviewImageB,
                R.id.newFavorPreviewImageC,
                R.id.newFavorPreviewImageD,
                R.id.newFavorPreviewImageE
        };
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    private void removeImage() {
        FragmentMessages fragmentMessages = FragmentMessages.getInstance();
        if (fragmentMessages.getDeleteIndex() >= 0) {
            imageList.remove(fragmentMessages.getDeleteIndex());
        }
    }

    public void setDate() {
        this.getActivity().showDialog(999, null);
    }

    public String GetDateText( ) {
        String am_pm_text = "AM";
        int display_hour =  this_hour;
        if( this_hour < 12) {
            if(this_hour == 0)
                display_hour = 12;
            this_am_pm = Calendar.AM;
            am_pm_text = "AM";
        }
        else {
            this_am_pm = Calendar.PM;
            display_hour = (this_hour - 12);
            if(this_hour == 12)
                display_hour = 12;
            else
                display_hour = (this_hour - 12);
            am_pm_text = "PM";
        }
        String display_minute = String.format("%02d",this_minute);
        return (this_month+1) + "/" + this_day + "/" + this_year + " " + display_hour + ":" + display_minute + " " + am_pm_text;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_new_favor, container, false);
        previewImage = (ImageView) fragmentView.findViewById(R.id.newFavorPreviewImage);
        loginPrefs = getActivity().getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        Button submitButton = (Button) fragmentView.findViewById(R.id.newFavorSubmitButton);
        ImageButton imageButton = (ImageButton) fragmentView.findViewById(R.id.newFavorBrowseButton);
        ImageButton imageDeadlineButton = (ImageButton) fragmentView.findViewById(R.id.newFavorDeadlineButton);
        deadlineText = (EditText) fragmentView.findViewById(R.id.newFavorDeadlineText);
        imagePreviews = new ImageView[] {
                (ImageView) fragmentView.findViewById(R.id.newFavorPreviewImage),
                (ImageView) fragmentView.findViewById(R.id.newFavorPreviewImageB),
                (ImageView) fragmentView.findViewById(R.id.newFavorPreviewImageC),
                (ImageView) fragmentView.findViewById(R.id.newFavorPreviewImageD),
                (ImageView) fragmentView.findViewById(R.id.newFavorPreviewImageE)
        };
        removeImage();
        for(int i = 0; i < maxImages; i++) {
            final int index = i;
            imagePreviews[i].setOnClickListener(
                    new ImageView.OnClickListener() {
                        public void onClick(View view) {
                            if (index < imageList.size()) {
                                Bitmap currentImage = imageList.get(index);
                                if (currentImage != null) {
                                    byte[] imageBytes = FavorsGlobal.getInstance().bitmapToByte(currentImage);
                                    ((UserActivity) getActivity()).goToDeleteImage(imageBytes, index);
                                }
                            }
                        }
                    }
            );
        }
        final Calendar c = Calendar.getInstance();
        this_day = c.get(Calendar.DAY_OF_MONTH);
        this_month = c.get(Calendar.MONTH);
        this_year = c.get(Calendar.YEAR);
        this_hour = c.get(Calendar.HOUR_OF_DAY);
        this_am_pm = c.get(Calendar.AM_PM);
        this_minute = c.get(Calendar.MINUTE);
        deadlineText.setText(GetDateText());
        submitButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        postFavorSync = new PostFavorSync();
                        postFavorSync.execute();
                    }
                });
        imageButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        galleryChooser();
                    }
                });
        imageDeadlineButton.setOnClickListener(
              new Button.OnClickListener() {
                  public void onClick(View view) {
                       // DateDialogFragment dateDialogFragment = new DateDialogFragment();
                      //dateDialogFragment.show(getFragmentManager(),"Pick Date");
                      DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                              new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int month, int day) {
                                        this_month = month;
                                        this_day = day;
                                        this_year = year;
                                        //deadlineText.setText((month+1) + "/" + day + "/" + year);
                                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                                                new TimePickerDialog.OnTimeSetListener() {
                                                    @Override
                                                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                                        this_hour = hourOfDay;
                                                        this_minute = minute;
                                                        deadlineText.setText(GetDateText());
                                                    }
                                                }, this_hour,this_minute,false
                                        );
                                        timePickerDialog.show();
                                    }
                              },this_year,this_month,this_day
                      );

                    datePickerDialog.show();
                  }

              });
        setPreviewImage();
        return fragmentView;
    }

    private void galleryChooser() {
        //Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if( imageList.size() >= maxImages)
            return;
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        //Intent chooser = Intent.createChooser(galleryIntent,"Select Photo");
        startActivityForResult(galleryIntent, 1);
    }

    private void setPreviewImage() {
        for(int i=0; i < imageList.size(); i++)
            try {
                Bitmap currentImage = imageList.get(i);
                int height = (int) (currentImage.getHeight() * (512.0 / currentImage.getWidth()));
                Bitmap newImage = Bitmap.createScaledBitmap(currentImage, 512, height, true);
                imagePreviews[i].setImageBitmap(newImage);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Uri photoUri = data.getData();
                    String[] projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(photoUri, projection, null, null, null);
                    cursor.moveToFirst();
                    int colIndex = cursor.getColumnIndex(projection[0]);
                    String path = cursor.getString(colIndex);
                    //InputStream imageStream = getActivity().getContentResolver().openInputStream(photoUri);
                    //Bitmap selectedBitmap = BitmapFactory.decodeStream(imageStream);
                    Bitmap selectedBitmap = BitmapFactory.decodeFile(path);
                    if (selectedBitmap != null)
                        imageList.add(selectedBitmap);
                    setPreviewImage();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public static class DateDialogFragment extends DialogFragment {

        public DateDialogFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.dialog_date, container);
            return view;
        }

    }

    public class PostFavorSync extends AsyncTask<String,String,String> {
        protected String status;


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            String sessionKey = loginPrefs.getString("sessionKey", null);
            status = "fail";
            EditText titleEditText = (EditText)getView().findViewById(R.id.newFavorTitleText);
            EditText descriptionEditText = (EditText)getView().findViewById(R.id.newFavorDescriptionText);
            EditText rewardEditText = (EditText)getView().findViewById(R.id.newFavorRewardText);
            EditText durationEditText = (EditText)getView().findViewById(R.id.newFavorDurationText);
            EditText participantText = (EditText)getView().findViewById(R.id.newFavorParticipantText);
            EditText locationText = (EditText)getView().findViewById(R.id.newFavorLocation);
            title = titleEditText.getText().toString();
            description = descriptionEditText.getText().toString();
            duration = durationEditText.getText().toString();
            reward = rewardEditText.getText().toString();
            participants = participantText.getText().toString();
            location = locationText.getText().toString();
            deadline = deadlineText.getText().toString();
            try {
                URL userInfoURL = new URL("http://45.55.133.160/favors/new_favor.html");
                HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
                //connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Cookie","sessionid=" + sessionKey);
                OutputStreamWriter outWriter = new OutputStreamWriter(connection.getOutputStream());
                String postString = "";
                postString += "title=" + title + "&";
                postString += "duration=" + duration + "&";
                postString += "description=" + description + "&";
                postString += "reward=" + reward + "&";
                postString += "number_of_participants=" + participants + "&";
                postString += "deadline=" + deadline + "&";
                postString += "zip=" + location;
                outWriter.write(postString);
                outWriter.close();
                int responseCode = connection.getResponseCode();
                BufferedReader inReader = new BufferedReader( new InputStreamReader(connection.getInputStream()));
                String line;
                StringBuffer response = new StringBuffer();
                while((line = inReader.readLine()) != null) {
                    status = line;
                    favorId = Integer.parseInt(status);
                    return line;
                }
                return "fail";
            } catch (MalformedURLException eURL) {
                return "fail";
            } catch (IOException e) {
                e.printStackTrace();
                return "fail";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if(favorId != - 1) {
                uploadImageSync = new UploadImageSync();
                uploadImageSync.execute();
            }
        }
    }


    public class UploadImageSync extends AsyncTask<String,String,String> {
        protected String status;

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            String sessionKey = loginPrefs.getString("sessionKey", null);
            String boundary = "*****";
            status = "fail";
            for( int i=0; i < imageList.size(); i++) {
                Bitmap currentImage = imageList.get(i);
                try {
                    URL userInfoURL = new URL("http://45.55.133.160/favors/upload_favor_photo.html");
                    HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
                    connection.setUseCaches(false);
                    connection.setDoOutput(true);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("Cache-Control", "no-cache");
                    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    String cookieString = "sessionid=" + sessionKey + ";favor_id=" + String.valueOf(favorId) + ";";
                    connection.setRequestProperty("Cookie", cookieString);
                    DataOutputStream request = new DataOutputStream(connection.getOutputStream());
                    request.writeBytes("--" + boundary + "\r\n");
                    request.writeBytes("Content-Disposition: form-data; name=\"photoFile\"; filename=\"test.bmp\"\r\n");
                    //request.writeBytes("Content-Type: Image/png\r\n");
                    request.writeBytes("\r\n");
                    //Conversion @ http://stackoverflow.com/questions/4989182/converting-java-bitmap-to-byte-array
                    ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                    if (currentImage != null && !currentImage.compress(Bitmap.CompressFormat.PNG, 100, byteStream)) {
                        status = "Invalid Image";
                        return "fail";
                    }
                    //Limit to 5 MB
                    byte[] byteArray = byteStream.toByteArray();
                    if (byteArray.length > 5000000) {
                        status = "Image is too large";
                        return "fail";
                    }
                    request.write(byteArray);
                    request.writeBytes("\r\n");
                    request.writeBytes("--" + boundary + "--" + "\r\n");
                    request.flush();
                    request.close();
                    int responseCode = connection.getResponseCode();
                    BufferedReader inReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while ((line = inReader.readLine()) != null) {
                        status = line;
                        //return line;
                    }
                } catch (MalformedURLException eURL) {
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return "out";
        }

        @Override
        protected void onPostExecute(String result) {
        }

    }


}

