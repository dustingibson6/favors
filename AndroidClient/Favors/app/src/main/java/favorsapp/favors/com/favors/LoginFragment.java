package favorsapp.favors.com.favors;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import favorsapp.favors.com.favors.sync.LoginSync;

public class LoginFragment extends Fragment implements OnTaskCompleted {

    private OnTransitionListener transitionCallback;
    private OnFragmentInteractionListener mListener;
    private SharedPreferences sharedPreferences;
    private OnTaskCompleted onTaskCompletedListener;
    public LoginSync loginSync;
    public String descriptionText;
    public String username;
    public String password;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public LoginFragment() {
        onTaskCompletedListener = (OnTaskCompleted)this;
    }

    public interface OnTransitionListener {
        public void onTransition();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    public int loginUser() {
        EditText usernameWidget = (EditText) getView().findViewById(R.id.loginUsernameText);
        EditText passwordWidget = (EditText) getView().findViewById(R.id.loginPasswordText);
        String verifyPasswordText = passwordWidget.getText().toString();
        String verifyUsernameText = usernameWidget.getText().toString();
        Button loginButton = (Button) getView().findViewById(R.id.loginButton);
        username = verifyUsernameText;
        password = verifyPasswordText;
        loginButton.setEnabled(false);
        loginButton.setText("Logging In...");
        loginSync = new LoginSync(onTaskCompletedListener);
        loginSync.execute(verifyUsernameText, verifyPasswordText);
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView =  inflater.inflate(R.layout.fragment_login, container, false);
        Button loginButton = (Button) fragmentView.findViewById(R.id.loginButton);
        Button cancelButton = (Button) fragmentView.findViewById(R.id.loginCancelButton);
        loginButton.setOnClickListener (
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        loginUser();
                    }
                });
        cancelButton.setOnClickListener (
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        getActivity().onBackPressed();
                    }
                });
        return fragmentView;
    }

    @Override
    public void onResume() {
        sharedPreferences = getActivity().getSharedPreferences("loginPrefs",Context.MODE_PRIVATE);
        super.onResume();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }


    public void holdOrientationControl() {
        FavorsGlobal.getInstance().holdOrientationControl(this);
    }

    public void releaseOrientationControl() {
        FavorsGlobal.getInstance().releaseOrientationControl(this);
    }

    public void getLoginStatus(String results) {
        TextView descriptionTextView = (TextView)getView().findViewById(R.id.loginDescriptionTextView);
        descriptionText = descriptionTextView.getText().toString();
        switch(results) {
            case "Username and Password Does not Match":
                descriptionText = "Username and Password Does not Match";
                break;

            case "fail":
                descriptionText = "Something is wrong with the server";
                break;

            default:
                try {
                    JSONArray loginArray = new JSONArray(results);
                    JSONObject currentObject = loginArray.getJSONObject(0);
                    String resultUserID = currentObject.getString("userid");
                    String resultSession = currentObject.getString("session");
                    String zip = currentObject.getString("zip");
                    Editor editor = sharedPreferences.edit();
                    String[] userInfoParams = results.split(",");
                    editor.putString("username", username);
                    editor.putString("password", password);
                    editor.putString("userID", resultUserID);
                    editor.putString("sessionKey", resultSession);
                    editor.putString("zip",zip);
                    descriptionText = "Logged In";
                    editor.commit();
                    this.getActivity().onBackPressed();
                } catch(JSONException jsonE) {
                    jsonE.printStackTrace();
                }
                break;
        }
        descriptionTextView.setTextColor(Color.RED);
        descriptionTextView.setText(descriptionText);
        Button loginButton = (Button)getView().findViewById(R.id.loginButton);
        loginButton.setEnabled(true);
        loginButton.setText("Login");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(loginSync != null)
            loginSync.cancel(true);
    }

    public void createSession(String username) {

    }

    public void UpdateUI(String status, String event) {
        releaseOrientationControl();
        getLoginStatus(status);
        this.getActivity().supportInvalidateOptionsMenu();
    }

    public void SetAutoComplete(String[] stringArray) {

    }

}
