package favorsapp.favors.com.favors;


public class FragmentMessages {
    private static FragmentMessages instance = new FragmentMessages();
    private int deleteIndex=-1;

    protected FragmentMessages() {
    }

    public static FragmentMessages getInstance() {
        return instance;
    }


    public int getDeleteIndex() {
        return deleteIndex;
    }

    public void setDeleteIndex(int index) {
        deleteIndex = index;
    }

}
