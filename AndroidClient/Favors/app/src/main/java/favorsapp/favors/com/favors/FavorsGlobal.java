package favorsapp.favors.com.favors;


import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class FavorsGlobal {

    private static FavorsGlobal instance = null;
    public String urlPrefix;

    static class DateBetween {
        int monthsBetween;
        int daysBetween;
        int hoursBetween;
        int minutesBetween;

        public DateBetween(int monthsBetween, int daysBetween, int hoursBetween, int minutesBetween) {
            this.monthsBetween = monthsBetween;
            this.daysBetween = daysBetween;
            this.hoursBetween = hoursBetween;
            this.minutesBetween = minutesBetween;
        }
    }

    public DateBetween GetDateBetween(DateTime A, DateTime B) {
        int monthsBetween = Months.monthsBetween(A, B).getMonths();
        int daysBetween = Days.daysBetween(A, B).getDays();
        int hoursBetween = Hours.hoursBetween(A, B).getHours();
        int minutesBetween = Minutes.minutesBetween(A, B).getMinutes();
        return new DateBetween(monthsBetween, daysBetween, hoursBetween, minutesBetween);
    }

    public String GetTimeLeftString(DateTime A, DateTime B) {
        String timeLeft = "";
        DateBetween d = GetDateBetween(A,B);
        if( d.monthsBetween < 0 || d.daysBetween < 0 || d.hoursBetween < 0 || d.minutesBetween < 0 )
            timeLeft = "Favor Already Started";
        else {
            if( d.monthsBetween > 0 ) {
                timeLeft += d.monthsBetween + " months ";
                B = new DateTime(B).minusMonths(d.monthsBetween);
                d = GetDateBetween(A, B);
            }
            if( d.daysBetween > 0 ) {
                timeLeft += d.daysBetween + " days ";
                B = new DateTime(B).minusDays(d.daysBetween);
                d = GetDateBetween(A, B);
            }
            if( d.hoursBetween > 0 ) {
                timeLeft += d.hoursBetween + " hours ";
                B = new DateTime(B).minusHours(d.hoursBetween);
                d = GetDateBetween(A, B);
            }
            if( d.minutesBetween > 0 )
                timeLeft += d.minutesBetween + " minutes ";
            timeLeft += "left";
        }
        return timeLeft;
    }

    public FavorsGlobal() {
        urlPrefix = "http://45.55.133.160/content/";
    }

    public static FavorsGlobal getInstance() {
        if(instance == null)
            instance = new FavorsGlobal();
        return instance;
    }

    public void holdOrientationControl(Fragment currentFragment) {
        int currentOrientation = currentFragment.getResources().getConfiguration().orientation;
        FragmentActivity currentActivity = currentFragment.getActivity();
        if (currentOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
            currentActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else
            currentActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    public void releaseOrientationControl(Fragment currentFragment) {
        int currentOrientation =  currentFragment.getResources().getConfiguration().orientation;
        FragmentActivity currentActivity = currentFragment.getActivity();
        currentActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    public byte[] bitmapToByte(Bitmap image) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public Drawable loadImageFromWeb(String imagePath) {
        String fullPath = urlPrefix + imagePath + ".png";
        if(imagePath == null)
            throw new NullPointerException();
        try {
            InputStream is = (InputStream) new URL(fullPath).getContent();
            Drawable draw = Drawable.createFromStream(is, "src");
            return draw;
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

    public Bitmap GetBitmapFromWeb(String imagePath) {
        String fullPath = urlPrefix + imagePath + ".png";
        try {
        URL url = new URL(fullPath);
        if(imagePath == null)
            throw new NullPointerException();
            InputStream in = url.openConnection().getInputStream();
            return BitmapFactory.decodeStream(in);
        } catch(MalformedURLException urlEx) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    public String getDisplayName(String userName, String firstName, String lastName) {
        String sender = "";
        if(firstName == null || firstName.equals(""))
            firstName = "null";
        if(lastName == null || lastName.equals(""))
            lastName = "null";
        if(firstName.equals("null") && lastName.equals("null"))
            sender = userName;
        else {
            if (!firstName.equals("null"))
                sender += firstName;
            if(!lastName.equals("null"))
                if (!sender.isEmpty())
                    sender += " " + lastName;
                else
                    sender += lastName;
        }
        return sender;
    }

}


