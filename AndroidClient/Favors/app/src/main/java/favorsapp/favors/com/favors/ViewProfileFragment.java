package favorsapp.favors.com.favors;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ViewProfileFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    //private GetProfileSync getProfileSync;
    protected SharedPreferences loginPrefs;
    protected String sessionKey;
    protected String profileName;
    protected LinearLayout progressLayout;

    public static ViewProfileFragment newInstance(String param1, String param2) {
        ViewProfileFragment fragment = new ViewProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ViewProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_view_profile, container, false);
        progressLayout = (LinearLayout) fragmentView.findViewById(R.id.viewProfileProgress);
        profileName = getArguments().getString("profile");
        //getProfileSync = new GetProfileSync();
        //getProfileSync.execute();
        return fragmentView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    public void setText(String jsonString) {
        try {
            JSONArray profileJSON = new JSONArray(jsonString);
            JSONObject currentObject = profileJSON.getJSONObject(0);
            TextView nameTextView = (TextView)getActivity().findViewById(R.id.viewProfileName);
            TextView qualificationsTextView = (TextView)getActivity().findViewById(R.id.viewProfileQualifications);
            ImageView profileImage = (ImageView)getActivity().findViewById(R.id.viewProfileImage);
            String lastName = currentObject.getString("lastname").equals("null") ? "" : currentObject.getString("lastname");
            String firstName = currentObject.getString("firstname").equals("null") ? "" : currentObject.getString("firstname");
            String photoID =  currentObject.getString("photo_id").equals("null") ? "default" : currentObject.getString("photo_id");
            String zip = currentObject.getString("zip").equals("null") ? "" : currentObject.getString("zip");
            String qualifications = currentObject.getString("qualifications").equals("null") ? "" : currentObject.getString("qualifications");
            String nameText = (FavorsGlobal.getInstance().getDisplayName(profileName,firstName,lastName));
            if(!zip.equals(""))
                nameText = nameText + ", " + zip;
            nameTextView.setText(nameText);
            profileImage.setImageDrawable(FavorsGlobal.getInstance().loadImageFromWeb(photoID));
            qualificationsTextView.setText(qualifications);
            //if(last)
        } catch(JSONException jsonE) {
            jsonE.printStackTrace();
            return;
        }
    }



}
