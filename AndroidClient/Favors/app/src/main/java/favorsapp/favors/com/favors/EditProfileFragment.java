package favorsapp.favors.com.favors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import favorsapp.favors.com.favors.sync.GetAutoLocationSync;
import favorsapp.favors.com.favors.sync.GetProfileSync;
import favorsapp.favors.com.favors.sync.SetProfileSync;

public class EditProfileFragment extends Fragment implements
        OnTaskCompleted, BeforeTaskExecution {

    private OnFragmentInteractionListener mListener;
    private OnTaskCompleted onTaskCompletedListener;
    private BeforeTaskExecution beforeTaskExecutionListener;
    private SharedPreferences loginPrefs;
    protected EditText lastNameEditText;
    protected EditText firstNameEditText;
    protected EditText qualificationsNameEditText;
    protected LinearLayout progressLayout;
    protected Button okButton;
    protected ImageView previewImage;
    protected AutoCompleteTextView autoLocationText;
    protected Bitmap profileImage;
    protected String sessionKey;
    protected String firstName;
    protected String lastName;
    protected String zip;
    protected String imagePath;
    protected String urlPrefix;
    protected String qualifications;
    protected String autoZip;
    protected ArrayAdapter<String> autoAdapter;
    public GetProfileSync getProfileSync;
    public SetProfileSync setProfileSync;
    public GetAutoLocationSync autoLocationSync;

    public static EditProfileFragment newInstance(String param1, String param2) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static EditProfileFragment newInstance(HomeActivity.RootFragmentListener fl) {
        EditProfileFragment fragment = new EditProfileFragment();
        return fragment;
    }

    public EditProfileFragment() {
        urlPrefix = "http://45.55.133.160/content/";
        onTaskCompletedListener = this;
        beforeTaskExecutionListener = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(setProfileSync != null)
            setProfileSync.cancel(true);
    }

    private void galleryChooser() {
        //Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        //Intent chooser = Intent.createChooser(galleryIntent,"Select Photo");
        startActivityForResult(galleryIntent, 1);
    }

    public void SetPreviewImage(Bitmap currentBitmap) {
        int height = (int) (currentBitmap.getHeight()*(512.0/currentBitmap.getWidth()));
        currentBitmap = Bitmap.createScaledBitmap(currentBitmap,512,height,true);
        previewImage.setImageBitmap(currentBitmap);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1) {
            if(resultCode == Activity.RESULT_OK) {
                Uri photoUri = data.getData();
               //previewImageView = (ImageView)getActivity().findViewById(R.id.editProfileImageView);
               String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(photoUri, projection, null, null, null);
                cursor.moveToFirst();
                int colIndex = cursor.getColumnIndex(projection[0]);
                String path = cursor.getString(colIndex);
                Bitmap selectedBitmap = BitmapFactory.decodeFile(path);
                profileImage = selectedBitmap;
                SetPreviewImage(selectedBitmap);
            }
        }
    }

    public void goToImageDelete() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView =  inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ViewGroup parent = (ViewGroup) fragmentView.getParent();
        if(parent != null)
            parent.removeView(fragmentView);
        progressLayout = (LinearLayout) fragmentView.findViewById(R.id.editProfileProgress);
        okButton = (Button) fragmentView.findViewById(R.id.okEditProfileButton);
        Button cancelButton = (Button) fragmentView.findViewById(R.id.editCancelButton);
        ImageButton browseButton = (ImageButton) fragmentView.findViewById(R.id.editProfileBrowseButton);
        previewImage = (ImageView) fragmentView.findViewById(R.id.editProfileImageView);
        autoLocationText = (AutoCompleteTextView) fragmentView.findViewById(R.id.autoZipText);
        lastNameEditText = (EditText)fragmentView.findViewById(R.id.editProfileLastName);
        firstNameEditText = (EditText)fragmentView.findViewById(R.id.editProfileFirstName);
        qualificationsNameEditText = (EditText)fragmentView.findViewById(R.id.editProfileQualifications);
        loginPrefs = getActivity().getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        sessionKey = loginPrefs.getString("sessionKey", null);
        getProfileSync = new GetProfileSync(beforeTaskExecutionListener,onTaskCompletedListener);
        getProfileSync.execute(sessionKey);
        autoLocationText.setThreshold(1);
        autoLocationText.setDropDownVerticalOffset(10);
        onTaskCompletedListener = this;
        okButton.setOnClickListener (
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        setProfileSync = new SetProfileSync(beforeTaskExecutionListener,onTaskCompletedListener,profileImage);
                        String firstName = firstNameEditText.getText().toString();
                        String lastName = lastNameEditText.getText().toString();
                        String location = autoLocationText.getText().toString();
                        String qualifcations = qualificationsNameEditText.getText().toString();
                        setProfileSync.execute(sessionKey,firstName,lastName,qualifcations,location);
                    }
                });
        browseButton.setOnClickListener (
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        galleryChooser();
                    }
                });
        cancelButton.setOnClickListener (
                new Button.OnClickListener() {
                    public void onClick(View view) {
                        getActivity().onBackPressed();
                    }
                });
        autoLocationText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                autoZip = autoLocationText.getText().toString();
                autoLocationSync = new GetAutoLocationSync(onTaskCompletedListener);
                autoLocationSync.execute(autoZip);
            }
        });
        //ProgressDialog.show(this.getActivity(),"test", "test", true);
        return fragmentView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    public boolean setText(String results) {
        FragmentActivity currentActivity = this.getActivity();
        switch(results) {
            case "Logged Out":
                //TODO: Configure UI to tell user is not logged in
                break;

            case "User Profile does not exist":
                break;

            case "Personal Info does not exist":
                break;

            case "fail":
                break;

            //TODO: Use default for general sever error
            default:
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("loginPrefs",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                try {
                    JSONArray infoArray = new JSONArray(results);
                    JSONObject currentObject = infoArray.getJSONObject(0);
                    firstName = currentObject.getString("firstname");
                    lastName = currentObject.getString("lastname");
                    zip = currentObject.getString("location");
                    imagePath = currentObject.getString("imagepath");
                    qualifications = currentObject.getString("qualifications");
                    lastNameEditText.setText(lastName);
                    firstNameEditText.setText(firstName);
                    autoLocationText.setText(zip);
                    qualificationsNameEditText.setText(qualifications);
                    editor.putString("zip", zip);
                    try {
                        Bitmap currentBitmap = FavorsGlobal.getInstance().GetBitmapFromWeb(imagePath);
                        SetPreviewImage(currentBitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
                } catch(JSONException jexc) {

                }
                break;
        }
        return false;
    }

    public void SetAutoComplete(String[] stringArray)
    {
        if(stringArray.length > 0) {
            autoAdapter = new ArrayAdapter<String>(getActivity(),
                    R.layout.list_autolocation,
                    R.id.autolocationText,
                    stringArray);
            autoLocationText.setAdapter(autoAdapter);
            if (!autoLocationText.isPopupShowing())
                autoLocationText.showDropDown();
        }
    }

    public void UpdateUI(String status, String event)
    {
        if(event.equals("Get Profile")) {
            setText(status);
            progressLayout.setVisibility(View.GONE);
        }
        else if(event.equals("Set Profile")) {
            okButton.setText("OK");
            if(!status.equals("fail"))
                getActivity().onBackPressed();

        }
    }

    public void PrepareExecution(String event) {
        if(event.equals("Get Profile")) {
            progressLayout.setVisibility(View.VISIBLE);
        }
        else if(event.equals("Set Profile")) {
            okButton.setText("POSTING...");
        }
    }



}
