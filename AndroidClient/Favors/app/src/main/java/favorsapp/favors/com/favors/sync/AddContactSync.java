package favorsapp.favors.com.favors.sync;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import favorsapp.favors.com.favors.BeforeTaskExecution;
import favorsapp.favors.com.favors.OnTaskCompleted;

public class AddContactSync extends AsyncTask<String,String,String> {
    protected String status;

    protected BeforeTaskExecution beforeTaskExecutionListener;
    protected OnTaskCompleted onTaskCompletedListener;
    protected String sessionKey;
    protected String zip;
    private Bitmap profileImage;

    public AddContactSync(OnTaskCompleted onTaskCompletedListener)
    {
        this.onTaskCompletedListener = onTaskCompletedListener;
        this.profileImage = profileImage;
    }

    @Override
    protected void onPreExecute() {
        //beforeTaskExecutionListener.PrepareExecution("Set Candidate");
    }

    @Override
    protected String doInBackground(String... params) {
        status = "fail";
        String sessionKey = params[0];
        String username = params[1];
        try {
            URL userInfoURL = new URL("http://45.55.133.160/favors/add_contact.html");
            HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Cookie","sessionid=" + sessionKey);
            OutputStreamWriter outWriter = new OutputStreamWriter(connection.getOutputStream());
            String postString = "";
            postString += "username=" + username;
            outWriter.write(postString);
            outWriter.close();
            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader inReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = inReader.readLine()) != null) {
                    status = line;
                    System.out.println(line);
                    return line;
                }
            }
            return "fail";
        } catch (MalformedURLException eURL) {
            return "fail";
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        onTaskCompletedListener.UpdateUI(status,"Set Contact");
    }
}
