package favorsapp.favors.com.favors.sync;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import favorsapp.favors.com.favors.BeforeTaskExecution;
import favorsapp.favors.com.favors.OnTaskCompleted;

public class GetProfileSync extends AsyncTask<String,String,String> {
    protected String status;
    protected OnTaskCompleted onTaskCompletedListener;
    protected BeforeTaskExecution beforeTaskExecutionListener;

    public GetProfileSync(BeforeTaskExecution beforeTaskExecutionListener, OnTaskCompleted onTaskCompletedListener)
    {
        this.beforeTaskExecutionListener = beforeTaskExecutionListener;
        this.onTaskCompletedListener = onTaskCompletedListener;
    }

    @Override
    protected void onPreExecute() {
        beforeTaskExecutionListener.PrepareExecution("Get Profile");
    }

    @Override
    protected String doInBackground(String... params) {
        status = "fail";
        String sessionKey = params[0];
        if(sessionKey == null) {
            status = "Logged Out";
            return "fail";
        }
        try {
            URL userInfoURL = new URL("http://45.55.133.160/favors/get_personal_info.html");
            HttpURLConnection connection = (HttpURLConnection) userInfoURL.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Cookie", "sessionid=" + sessionKey);
            int responseCode = connection.getResponseCode();
            BufferedReader inReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = inReader.readLine()) != null) {
                status = line;
                return line;
            }
            connection.disconnect();
            return "fail";
        } catch (MalformedURLException eURL) {
            return "fail";
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        onTaskCompletedListener.UpdateUI(status,"Get Profile");
    }
}